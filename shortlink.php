<html>
<body>
<?php
/*
Template Name: shortlink
*/
/**
 * XClusive - template for displaying all pages
 *
 * @file page.php
 * @package xclusive 
 * @author Hafid Trujillo
 * @copyright 2013 Xpark Media
 * @license license.txt 
 * @version release: 0.1.0
 * @filesource  wp-content/themes/xclusive/page.php
 * @since available since 0.1.0
 */
 
  get_header(); ?>
  

		
			<div id="content" role="main">
			
				
               
			
		<?php echo do_shortcode('[easy_contact_forms fid=2]'); ?>	
			
		</div><!--#content-->
	</section><!--#primary-->

 </body>
 </html>
