<?php
/**
 * Template Name: New Try
 *
 * @file page.php
 * @package xclusive 
 * @author Hafid Trujillo
 * @copyright 2013 Xpark Media
 * @license license.txt 
 * @version release: 0.1.0
 * @filesource  wp-content/themes/xclusive/page.php
 * @since available since 0.1.0
 */
 
  get_header(); ?>

	<section id="primary">
		<div id="content" role="main">
			
		<?php echo do_shortcode('shortcode option1=" [maxbutton id="1"] " '); ?>	
			
		</div><!--#conten//t-->
	</section><!--#primary-->

  <?php get_footer(); ?>