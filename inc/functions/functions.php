<?php
/**
 * Interface functions and definitions
 *
 * This file contains all the functions and it's defination that particularly can't be
 * in other files.
 *
 * @package Theme Horse
 * @subpackage Interface
 * @since Interface 1.0
 */

/****************************************************************************************/

add_action( 'wp_enqueue_scripts', 'interface_scripts_styles_method' );
/**
 * Register jquery scripts
 */
function interface_scripts_styles_method() {

	global $options, $array_of_default_settings;
	$options = wp_parse_args( get_option( 'interface_theme_options', array() ), interface_get_option_defaults() );

	/**
	 * Loads our main stylesheet.
	 */
	// Load our main stylesheet.
	wp_enqueue_style( 'interface_style', get_stylesheet_uri() );


	wp_style_add_data( 'interface-ie', 'conditional', 'lt IE 9' );

	if ( 'on' == $options['site_design'] ) {
		//Css for responsive design
		wp_enqueue_style( 'interface-responsive', get_template_directory_uri() . '/css/responsive.css' );
	}
	/**
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/**
	 * Register JQuery cycle js file for slider.
	 * Register Jquery fancybox js and css file for fancybox effect.
	 */
	wp_register_script( 'jquery_cycle', INTERFACE_JS_URL . '/jquery.cycle.all.min.js', array( 'jquery' ), '2.9999.5', true );

	wp_register_style( 'google_fonts', '//fonts.googleapis.com/css?family=PT+Sans:400,700italic,700,400italic' );


	/**
	 * Enqueue Slider setup js file.
	 * Enqueue Fancy Box setup js and css file.
	 */
	if ( ( is_home() || is_front_page() ) && 1 != $options['disable_slider'] ) {
		wp_enqueue_script( 'interface_slider', INTERFACE_JS_URL . '/interface-slider-setting.js', array( 'jquery_cycle' ), false, true );
	}

	wp_enqueue_script( 'backtotop', INTERFACE_JS_URL . '/backtotop.js', array( 'jquery' ) );
	wp_enqueue_script( 'scripts', INTERFACE_JS_URL . '/scripts.js', array( 'jquery' ) );

	wp_enqueue_style( 'google_fonts' );

}

/****************************************************************************************/

function interface_add_editor_styles() {
	$font_url = str_replace( ',', '%2C', '//fonts.googleapis.com/css?family=PT+Sans:400,700italic,700,400italic' );
	add_editor_style( $font_url );
}

add_action( 'after_setup_theme', 'interface_add_editor_styles' );

/****************************************************************************************/

add_action( 'admin_print_scripts', 'interface_media_js', 10 );
/**
 * Register scripts for image upload
 *
 * @uses wp_register_script
 * Hooked to admin_print_scripts action hook
 */
function interface_media_js() {

	wp_enqueue_script( 'interface_meta_upload_widget', INTERFACE_ADMIN_JS_URL . '/add-image-script-widget.js', array(
		'jquery',
		'media-upload',
		'thickbox'
	) );
	wp_enqueue_style( 'thickbox' );

}


/****************************************************************************************/

add_filter( 'wp_page_menu', 'interface_wp_page_menu' );
/**
 * Remove div from wp_page_menu() and replace with ul.
 * @uses wp_page_menu filter
 */
function interface_wp_page_menu( $page_markup ) {
	preg_match( '/^<div class=\"([a-z0-9-_]+)\">/i', $page_markup, $matches );
	$divclass   = $matches[1];
	$replace    = array( '<div class="' . $divclass . '">', '</div>' );
	$new_markup = str_replace( $replace, '', $page_markup );
	$new_markup = preg_replace( '/^<ul>/i', '<ul class="' . $divclass . '">', $new_markup );

	return $new_markup;
}

/****************************************************************************************/

if ( ! function_exists( 'interface_pass_slider_effect_cycle_parameters' ) ) :
	/**
	 *Functions that Passes slider effect  parameters from php files to jquery file.
	 */
	function interface_pass_slider_effect_cycle_parameters() {

		global $options, $array_of_default_settings;
		$options = wp_parse_args( get_option( 'interface_theme_options', array() ), interface_get_option_defaults() );

		$transition_effect   = $options['transition_effect'];
		$transition_delay    = $options['transition_delay'] * 1000;
		$transition_duration = $options['transition_duration'] * 1000;
		wp_localize_script(
			'interface_slider',
			'interface_slider_value',
			array(
				'transition_effect'   => $transition_effect,
				'transition_delay'    => $transition_delay,
				'transition_duration' => $transition_duration
			)
		);

	}
endif;

/****************************************************************************************/

add_filter( 'excerpt_length', 'interface_excerpt_length' );
/**
 * Sets the post excerpt length to 50 words.
 *
 * function tied to the excerpt_length filter hook.
 *
 * @uses filter excerpt_length
 */
function interface_excerpt_length( $length ) {
	return 50;  // this will return 50 words in the excerpt
}

add_filter( 'excerpt_more', 'interface_continue_reading' );
/**
 * Returns a "Continue Reading" link for excerpts
 */
function interface_continue_reading() {
	return '&hellip; ';
}


add_filter( 'body_class', 'interface_body_class' );
/**
 * Filter the body_class
 *
 * Throwing different body class for the different layouts in the body tag
 */
function interface_body_class( $classes ) {
	global $post;
	global $options, $array_of_default_settings;
	$options = wp_parse_args( get_option( 'interface_theme_options', array() ), interface_get_option_defaults() );

	if ( $post ) {
		$layout = get_post_meta( $post->ID, 'interface_sidebarlayout', true );
	}
	if ( empty( $layout ) || is_archive() || is_search() || is_home() ) {
		$layout = 'default';
	}
	if ( 'default' == $layout ) {

		$themeoption_layout = $options['default_layout'];

		if ( 'left-sidebar' == $themeoption_layout ) {
			$classes[] = 'left-sidebar-template';
		} elseif ( 'right-sidebar' == $themeoption_layout ) {
			$classes[] = '';
		} elseif ( 'no-sidebar-full-width' == $themeoption_layout ) {
			$classes[] = 'full-width-template';
		} elseif ( 'no-sidebar' == $themeoption_layout ) {
			$classes[] = 'no-sidebar-template';
		}
	} elseif ( 'left-sidebar' == $layout ) {
		$classes[] = 'left-sidebar-template';
	} elseif ( 'right-sidebar' == $layout ) {
		$classes[] = ''; //css blank
	} elseif ( 'no-sidebar-full-width' == $layout ) {
		$classes[] = 'full-width-template';
	} elseif ( 'no-sidebar' == $layout ) {
		$classes[] = 'no-sidebar-template'; //css for no-sidebar-template from <body >
	}
	if ( is_home() || is_front_page() ) {

		if ( is_page_template( 'page-templates/page-template-business.php' ) ) {

			$classes[] = 'business-layout';
		} else {
			$classes[] = '';        // css for home page with body class.
		}
	}

	if ( is_page_template( 'page-templates/page-template-blog-image-medium.php' ) ) {
		$classes[] = 'blog-medium';
	}
	if ( 'narrow-layout' == $options['site_layout'] ) {
		$classes[] = 'narrow-layout';
	}

	return $classes;
}

/****************************************************************************************/

add_action( 'wp_head', 'interface_internal_css' );
/**
 * Hooks the Custom Internal CSS to head section
 */
function interface_internal_css() {


	global $options, $array_of_default_settings;
	$options = wp_parse_args( get_option( 'interface_theme_options', array() ), interface_get_option_defaults() );

	if ( ! empty( $options['custom_css'] ) ) {
		$interface_internal_css = '<!-- ' . get_bloginfo( 'name' ) . ' Custom CSS Styles -->' . "\n";
		$interface_internal_css .= '<style type="text/css" media="screen">' . "\n";
		$interface_internal_css .= $options['custom_css'] . "\n";
		$interface_internal_css .= '</style>' . "\n";
	}

	if ( isset( $interface_internal_css ) ) {
		echo $interface_internal_css;
	}
}

/****************************************************************************************/
add_action( 'pre_get_posts', 'interface_alter_home' );
/**
 * Alter the query for the main loop in home page
 *
 * @uses pre_get_posts hook
 */
function interface_alter_home( $query ) {
	global $options, $array_of_default_settings;
	$options = wp_parse_args( get_option( 'interface_theme_options', array() ), interface_get_option_defaults() );
	$cats    = $options['front_page_category'];

	if ( $options['exclude_slider_post'] != 0 && ! empty( $options['featured_post_slider'] ) ) {
		if ( $query->is_main_query() && $query->is_home() ) {
			$query->query_vars['post__not_in'] = $options['featured_post_slider'];
		}
	}

	if ( ! in_array( '0', $cats ) ) {
		if ( $query->is_main_query() && $query->is_home() ) {
			$query->query_vars['category__in'] = $options['front_page_category'];
		}
	}
}

/****************************************************************************************/

add_filter( 'wp_page_menu', 'interface_wp_page_menu_filter' );
/**
 * @uses wp_page_menu filter hook
 */
if ( ! function_exists( 'interface_wp_page_menu_filter' ) ) {
	function interface_wp_page_menu_filter( $text ) {
		$replace = array(
			'current_page_item' => 'current-menu-item'
		);

		$text = str_replace( array_keys( $replace ), $replace, $text );

		return $text;
	}
}
/**************************************************************************************/

function interface_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Lato, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Lato font: on or off', 'interface' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Lato:300,400,700,900,300italic,400italic,700italic' ), "//fonts.googleapis.com/css" );
	}

	return $font_url;
}

function interface_sell_books_shortcode( $atts ) {
	$atts = shortcode_atts( array(
		'foo' => 'no foo',
		'baz' => 'default baz'
	), $atts, 'sell_books' );

	$is_calledback = sell_books_form_callback();

	if ( ! $is_calledback ) {
		ob_start();

		?>
		<div class="book-sell-primary">
			<div class="one-half">
				<ul class="unstyled hero_benefits">
					<li class="book">
						<h3><span class="badge badge-success">1</span> GET A QUOTE</h3>Type the ISBNs from
						your books into the form.
					</li>

					<li class="ship">
						<h3><span class="badge badge-success">2</span> MEET UP</h3>Meet up with a student sales
						representative who will purchase your books.
					</li>

					<li class="cash">
						<h3><span class="badge badge-success">3</span> GET CASH</h3>Get cash on the spot for your books
					</li>
				</ul>
			</div>
			<div class="one-half">
				<form id="price-books-form" name="price-books-form" action="<?php echo get_permalink(); ?>"
				      method="post">

					<?php if ( isset( $_POST['error'] ) ) {
						?><span class="error"><?php echo $_POST['error']; ?></span><?php
					} ?>

					<textarea class="txt_isbns" required
					          placeholder="Click to enter ISBNs Example: 9783593812489,9784930948173,9784829577392,9783840948274"
					          name="isbns" row="15" class="input-block-level"></textarea>

					<input class="buy_GetOffer" type="submit" name="GetOffer" value="Get Prices »">
				</form>
			</div>
		</div>
		<?php
		return ob_get_clean();
	}

	return '';
}

function get_book_details_amazon( $isbnList, $last_id = 0 ) {

	$offer_log = new Offer_Log();

	//get the amazon data for the isbns
	$CashOfferListGood   = array();
	$CashOfferListAccept = array();
	$CashOfferListTxt    = array();
	$LowesetUsed         = array();
	$SalesRank           = array();
	$Image               = array();
	$Title               = array();
	$Edition             = array();
	$Author              = array();
	$XmlOutput           = "";
	$ReturnedCount       = 0;

	//loop thru the list in batches of 10 until they are all processed
	for ( $TenCount = 0; $TenCount < ( sizeof( $isbnList ) / 10 ); $TenCount ++ ) {
		if ( $debug ) {
			print "<br>TenCount=$TenCount.";
		}

		//get a batch of 10
		$isbn10  = array();        //holds the current 10 isbns to be looked up
		$isbnstr = "";    //holds the current 10 isbns in a string that will be added to the request url later
		//get the current 10 from isbnList
		for ( $i = 0; $i < 10; $i ++ ) {
			$index = ( ( $TenCount * 10 ) + ( $i ) );
			if ( $index < sizeof( $isbnList ) ) {
				$isbn10[] = $isbnList[ ( ( $TenCount * 10 ) + ( $i ) ) ];
				$isbnstr .= $isbnList[ ( ( $TenCount * 10 ) + ( $i ) ) ] . " or ";
			} else {
				break;    // last batch of 10 which probably has less than 10
			}
		}
		$isbnstr = substr( $isbnstr, 0, ( strlen( $isbnstr ) - 4 ) );        //remove last " or "
		$isbnstr = str_replace( ",", "%2C", $isbnstr );


		//prepare and execute the amazon api call
		$Locale = "amazon.com";

		/*$AWSAccessKeyId = "AKIAIPSYVM2FXZ3AHHTQ";
		$SecretAccessKey = "Xk1E/PZHwUf1giCcWT9sqgkAzagX4/zNG2XEhm0N";
		#above are your keys and below is your user login name
		$AssociateTag = "allegracorpco-20";/*/

		$AWSAccessKeyId  = "AKIAIDFLEXUXQGAE6PRQ";
		$SecretAccessKey = "g99MawBR5NDOrgiN16G97nEaHEipkgxGLvGJMb+W";
		#above are your keys and below is your user login name
		$AssociateTag = "360buyback-20";

		//$ItemId = "0679722769"; // ASIN
		//$upc="012569768949";

		$Timestamp = gmdate( "Y-m-d\TH:i:s\Z" );
		//print $Timestamp; exit;
		$Timestamp = str_replace( ":", "%3A", $Timestamp );
		//$ResponseGroup = "ItemAttributes,Offers,Images,Reviews";
		//$ResponseGroup = "SalesRank,ItemAttributes,OfferSummary";
		$ResponseGroup = "SalesRank,ItemAttributes,OfferSummary,Images";
		$ResponseGroup = str_replace( ",", "%2C", $ResponseGroup );

		//notice above and below parms are in alphabetical order

		//$String="AWSAccessKeyId=" . $AWSAccessKeyId . "&AssociateTag=" . $AssociateTag . "&IdType=" . $IdType . "&ItemId=" . $IdTypeValue . "&Operation=ItemLookup&ResponseGroup=" . $ResponseGroup . "&SearchIndex=DVD&Service=AWSECommerceService&Timestamp=" . $Timestamp . "&Version=2011-08-01";
		//used all below
		$String = "AWSAccessKeyId=" . $AWSAccessKeyId . "&AssociateTag=" . $AssociateTag . "&IdType=" . $IdType . "&ItemId=" . $IdTypeValue . "&Operation=ItemLookup&ResponseGroup=" . $ResponseGroup . "&SearchIndex=All&Service=AWSECommerceService&Timestamp=" . $Timestamp . "&Version=2011-08-01";
		//above is what was in php program, below was in python program, since you are looking up 10, use below and convert to php
		//String="AWSAccessKeyId="+AWS_ACCESS_KEY_ID+"&AssociateTag=ASSOCIATE_TAG&Operation=ItemSearch&Power=ISBN:"+upcs+"&ResponseGroup="+ResponseGroup+"&SearchIndex=Books&Service=AWSECommerceService&Timestamp="+Timestamp+"&Version=2009-01-06"
		$String = "AWSAccessKeyId=" . $AWSAccessKeyId . "&AssociateTag=" . $AssociateTag . "&Operation=ItemSearch&Power=ISBN:" . $isbnstr . "&ResponseGroup=" . $ResponseGroup . "&SearchIndex=Books&Service=AWSECommerceService&Timestamp=" . $Timestamp . "&Version=2011-08-01";

		$String = str_replace( "\n", "", $String );
		//i added below from searchi when this code wasn't working, but I don't see any of the below chars in $String
		//but there must have been something because now this fucker works again, wtf!
		$String = str_replace( "+", "%2B", $String );
		$String = str_replace( ":", "%3A", $String );        //I put next 2 in for Power search
		$String = str_replace( "*", "%2A", $String );
		$String = str_replace( " ", "%20", $String );

		$Prepend       = "GET\nwebservices." . $Locale . "\n/onca/xml\n";
		$PrependString = $Prepend . $String;

		$Signature = base64_encode( hash_hmac( "sha256", $PrependString, $SecretAccessKey, true ) );
		$Signature = str_replace( "+", "%2B", $Signature );
		$Signature = str_replace( "=", "%3D", $Signature );
		//not sure if below (and above) should be all done before/after/both/split.  I tried all before and after and it failed.
		//todo, below are not in Python
		$Signature = str_replace( ":", "%3A", $Signature );
		$Signature = str_replace( "*", "%2A", $Signature );

		$BaseUrl       = "http://webservices." . $Locale . "/onca/xml?";
		$SignedRequest = $BaseUrl . $String . "&Signature=" . $Signature;

		//$XML = simplexml_load_file($SignedRequest);

		//echo '<a href="'.$SignedRequest.'">XML</a><p>';
		$vurl = $SignedRequest;
		//print $vurl;
		//exit;
		//print_r ($XML);

		$curpage = @file_get_contents( $vurl );
		$XmlOutput .= $curpage;

		/*for some reason above fails, but if I put vurl into browser, I get below?  Why? To see vurl, remove the @ above
		< ?xml version="1.0"? >
		<ItemLookupErrorResponse xmlns="http://ecs.amazonaws.com/doc/2011-08-01/"><Error><Code>AccountLimitExceeded</Code><Message>Account limit of 2000 requests per hour exceeded.</Message></Error><RequestID>093bc783-c9fd-4de0-ae71-84f20da5ab51</RequestID></ItemLookupErrorResponse>
		//*/


		if ( $curpage == false ) {
			//should never happen.  could keep going, but if this fails, amazon is down, your account is revoked, etc.  no sense in continuing.
			print '<P><B>ERROR,Please refresh your page until price quote comes up, this may take multiple attempts.  If that does not work please email your ISBNS to <a href="mailto:service@360buyback.com" >service@360buyback.com</a>  We have been experiencing technical difficulties the past few days.  We will look up your ISBNS manually and email you a quote.  Sorry for the inconvenience.';
			exit;
		}


		//print "<br>Processing amazon query results ...";
		//process the amazon query results, parse the returned xml getting used price and rank for the 10 upcs request you just sent


		/*
		break up xml by each returned upc and search in that range for above

		could search backwards thru the results with $pos = stripos(strrev($haystack), strrev($needle)); but then lose correct position number?

		the returned xml has header stuff, and then the 10 blocks can be broken down into:

		<Item><ASIN>B001KKU9GE</ASIN><DetailPageURL>http://www.amazon.com/10-Dead-Men-Doug-Bradley/dp/B001KKU9GE%3FSubscriptionId%3DAKIAI3ISYLSZ6ENOPKKQ%26tag%3Dallegracorpco-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB001KKU9GE</DetailPageURL>

		<SalesRank>91716</SalesRank>

		<ItemAttributes><Actor>Doug Bradley</Actor><Actor>Pooja Shah</Actor><Actor>Brendan Carr</Actor><Actor>Terry Stone</Actor><Actor>Keith Eyles</Actor><AspectRatio>1.77:1</AspectRatio><AudienceRating>Unrated</AudienceRating><Binding>DVD</Binding><Brand>MTI PRODUCTIONS</Brand><Director>Ross Boyask</Director><EAN>0039414521207</EAN><Format>AC-3</Format><Format>Color</Format><Format>Dolby</Format><Format>DVD</Format><Format>Subtitled</Format><Format>Widescreen</Format><Format>NTSC</Format><Label>Mti Home Video</Label><Languages><Language><Name>English</Name><Type>Original Language</Type></Language><Language><Name>Spanish</Name><Type>Subtitled</Type></Language></Languages><ListPrice><Amount>2495</Amount><CurrencyCode>USD</CurrencyCode><FormattedPrice>$24.95</FormattedPrice></ListPrice><Manufacturer>Mti Home Video</Manufacturer><MPN>D2120D</MPN><NumberOfItems>1</NumberOfItems><PackageDimensions><Height Units="hundredths-inches">58</Height><Length Units="hundredths-inches">710</Length><Weight Units="hundredths-pounds">18</Weight><Width Units="hundredths-inches">542</Width></PackageDimensions><ProductGroup>DVD</ProductGroup><Publisher>Mti Home Video</Publisher><ReleaseDate>2009-01-27</ReleaseDate><RunningTime Units="minutes">90</RunningTime><Studio>Mti Home Video</Studio><TheatricalReleaseDate>2007</TheatricalReleaseDate><Title>10 Dead Men</Title>

		<UPC>039414521207</UPC>

		</ItemAttributes><OfferSummary><LowestNewPrice><Amount>1238</Amount><CurrencyCode>USD</CurrencyCode><FormattedPrice>$12.38</FormattedPrice></LowestNewPrice>

		<LowestUsedPrice><Amount>388</Amount>

		<CurrencyCode>USD</CurrencyCode>
		<FormattedPrice>$3.88</FormattedPrice>
		</LowestUsedPrice><TotalNew>38</TotalNew><TotalUsed>15</TotalUsed><TotalCollectible>0</TotalCollectible><TotalRefurbished>0</TotalRefurbished></OfferSummary></Item>

		//*/


		//loop thru the 10 isbns which were looked up on amazon and save the results to arrays
		for ( $i = 0; $i < sizeof( $isbn10 ); $i ++ ) {

			//the Amazon data is not always returned in the same order as the isbns that were submitted.  so lookup the isbns in the output in the same order they were entered so your looked up Amazon data is in the correct order

			if ( $debug ) {
				print "<P>searching for " . '&lt;EAN&gt;' . $isbn10[ $i ] . '&lt;/EAN&gt;';
			}
			/*
			print "<br>searching for " . '<EAN>' . $isbn10[$i] . '</EAN>';
			$vsoibisbn = stripos($curpage, '<ISBN>' . $isbn10[$i] . '</ISBN>', $veoib);
			//it appears they strip off the 978 in the isbn tag, but the whole number is in the ean tag so just search for below instead*/
			//$vsoibisbn = stripos($curpage, '<EAN>' . $isbn10[$i] . '</EAN>', $veoib);
			$vsoibisbn = stripos( $curpage, '<EAN>' . $isbn10[ $i ] . '</EAN>' );
			if ( $vsoibisbn === false ) {
				$vsoibisbn = stripos( $curpage, '<ISBN>' . $isbn10[ $i ] . '</ISBN>' );
				if ( $vsoibisbn === false ) {
					//print '<P><B>====ERROR, ' . $isbn10[$i] . ' not found.</B>';
					$LowesetUsed[] = "";
					$SalesRank[]   = "";
					$Image[]       = "";
					//$Title[]="ISBN " . $isbn10[$i] . " invalid/school specific version, please verify the ISBN entered is correct.";
					//Edited error message on 4/22/14
					$Title[]   = " " . " Invalid ISBN, please double check the number. This error will also appear for UNF specific editions.";
					$Edition[] = "";
					$Author[]  = "";
					continue;
				}
			}

			//$vsoib = strrpos($curpage, '<Item>', $vsoibisbn);
			//strrpos is very confusing, offeset must be - to search backwards, but it is the number of chars from the END of the string
			$vsoib = strrpos( $curpage, '<Item>', - ( strlen( $curpage ) - $vsoibisbn ) );
			if ( $vsoib === false ) {
				print '<P><B>====ERROR, &lt;Item&gt; not found.</B>';
				$LowesetUsed[] = "";
				$SalesRank[]   = "";
				$Image[]       = "";
				continue;
			}

			#find end of this item block so you can verify you stay within it when searching
			$veoib = stripos( $curpage, '</Item>', $vsoib );
			if ( $veoib === false ) {
				print '<P><B>====ERROR, &lt;/Item&gt; not found.</B>';
				$LowesetUsed[] = "";
				$SalesRank[]   = "";
				$Image[]       = "";
				continue;
			}

			$ItemBlock = substr( $curpage, $vsoib, ( $veoib - $vsoib ) );
			//print "<br>vsoib=$vsoib, veoib=$veoib. vsoibisbn=$vsoibisbn"; // . substr($curpage, ($vsoib+11), ($vsoib - ($veoib+11) ) );

			//you may want to move ReturnedCount to after below fields are found, or just keep hear since the Item was found
			$ReturnedCount += 1;

			//get the sales rank
			$vstart = stripos( $ItemBlock, '<SalesRank>' );
			if ( $vstart === false ) {
				print '<P><B>====ERROR, &lt;SalesRank&gt; not found, skipping</B>';
				$SalesRank[] = "";
			} else {
				$vend = stripos( $ItemBlock, '</SalesRank>' );
				if ( $vstart === false ) {
					print '<P><B>====ERROR, &lt;/SalesRank&gt; not found, skipping</B>';
					$SalesRank[] = "";
				} else {
					$SalesRank[] = substr( $ItemBlock, ( $vstart + 11 ), ( $vend - ( $vstart + 11 ) ) );
				}
			}
			if ( $debug ) {
				print "<br>SalesRank=" . $SalesRank[ $i ] . ".";
			}


			//get the lowest used price
			$vstart = stripos( $ItemBlock, '<LowestUsedPrice>' );
			if ( $vstart === false ) {
				print '<P><B>====ERROR,<LowestUsedPrice&gt; not found, skipping</B>';
				$LowesetUsed[] = "";
			} else {
				$vstart = stripos( $ItemBlock, '<FormattedPrice>', $vstart );
				if ( $vstart === false ) {
					print '<P><B>====ERROR, &lt;FormattedPrice&gt; not found, skipping</B>';
					$LowesetUsed[] = "";
				} else {
					$vend = stripos( $ItemBlock, '</FormattedPrice>', $vstart );
					if ( $vstart === false ) {
						print '<P><B>====ERROR, &lt;/FormattedPrice&gt; not found, skipping</B>';
						$LowesetUsed[] = "";
					} else {
						//added one to strlen below to skip the leading $ in formatted price
						$LowesetUsed[] = str_replace( ',', '', substr( $ItemBlock, ( $vstart + 17 ), ( $vend - ( $vstart + 17 ) ) ) );

						/*python code for euros which i don't think we have to worry about euros
						#euros are 1.999,99 format so might as well fix this here
						if (curpage[(vend-3):(vend-2)]==","):
							vfieldsval[vfields[k]]=curpage[(vstart+17):(vend-3)].replace('.', '')+'.'+curpage[(vend-2):vend]
						else:
							vfieldsval[vfields[k]]=curpage[(vstart+17):vend].replace(',', '')
						//*/
					}
				}
			}
			if ( $debug ) {
				print "<br>UsedPrice=" . $LowesetUsed[ $i ] . ".";
			}


			//get the image
			$vstart = stripos( $ItemBlock, '<MediumImage><URL>' );
			if ( $vstart === false ) {
				print '<P><B>====ERROR, &lt;MediumImage&gt; not found, skipping</B>';
				$Image[] = "";
			} else {
				$vend = stripos( $ItemBlock, '</URL>', $vstart );
				if ( $vstart === false ) {
					print '<P><B>====ERROR, &lt;/URL&gt; not found, skipping</B>';
					$Image[] = "";
				} else {
					$Image[] = substr( $ItemBlock, ( $vstart + 18 ), ( $vend - ( $vstart + 18 ) ) );
				}
			}
			if ( $debug ) {
				print "<br>Image=" . $Image[ $i ] . ".";
			}


			//get the Title
			$vstart = stripos( $ItemBlock, '<Title>' );
			if ( $vstart === false ) {
				print '<P><B>====ERROR, &lt;Title&gt; not found, skipping</B>';
				$Title[] = "";
			} else {
				$vend = stripos( $ItemBlock, '</Title>', $vstart );
				if ( $vstart === false ) {
					print '<P><B>====ERROR, &lt;/Title&gt; not found, skipping</B>';
					$Title[] = "";
				} else {
					$Title[] = substr( $ItemBlock, ( $vstart + 7 ), ( $vend - ( $vstart + 7 ) ) );
				}
			}
			if ( $debug ) {
				print "<br>Title=" . $Title[ $i ] . ".";
			}


			//get the Edition
			$vstart = stripos( $ItemBlock, '<Edition>' );
			if ( $vstart === false ) {
				print '<P><B>====ERROR, &lt;Edition&gt; not found, skipping</B>';
				$Edition[] = "";
			} else {
				$vend = stripos( $ItemBlock, '</Edition>', $vstart );
				if ( $vstart === false ) {
					print '<P><B>====ERROR, &lt;/Edition&gt; not found, skipping</B>';
					$Edition[] = "";
				} else {
					$Edition[] = substr( $ItemBlock, ( $vstart + 9 ), ( $vend - ( $vstart + 9 ) ) );
				}
			}
			if ( $debug ) {
				print "<br>Edition=" . $Edition[ $i ] . ".";
			}


			//get the Author
			$vstart = stripos( $ItemBlock, '<Author>' );
			if ( $vstart === false ) {
				print '<P><B>====ERROR, &lt;Author&gt; not found, skipping</B>';
				$Author[] = "";
			} else {
				$vend = stripos( $ItemBlock, '</Author>', $vstart );
				if ( $vstart === false ) {
					print '<P><B>====ERROR, &lt;/Author&gt; not found, skipping</B>';
					$Author[] = "";
				} else {
					$Author[] = substr( $ItemBlock, ( $vstart + 8 ), ( $vend - ( $vstart + 8 ) ) );
				}
			}
			if ( $debug ) {
				print "<br>Author=" . $Author[ $i ] . ".";
			}


			/*keep these around in case you need them later
			//get the total used
			$vstart = stripos($curpage, '<TotalUsed>', $vsoib);
			if ($vstart === false) {
				print '<P><B>====ERROR, &lt;TotalUsed&gt; not found, skipping</B>';
			}else{
				$vend = stripos($curpage, '</TotalUsed>', $vstart);
				if ($vstart === false) {
					print '<P><B>====ERROR, &lt;/TotalUsed&gt; not found, skipping</B>';
				}else{
					$totalused=substr($curpage, ($vstart+11), ($vend - ($vstart+11) ) );
				}
			}
			//print "<P>rank=" . $rank . ".";

			//*/

		} //for ($i=0; $i<sizeof($isbn10); $i++) {	//loop thru the 10 isbns which were looked up on amazon and save the results to arrays
	} //for ($TenCount=0; $TenCount<(sizeof($isbnList)/10); $TenCount++) {	//loop thru the list in batches of 10 until they are all processed


	$OfferTotalGood   = 0;
	$OfferTotalAccept = 0;
	//compute offer and display on the screen
	for ( $i = 0; $i < sizeof( $isbnList ); $i ++ ) {

		if ( $debug ) {
			print "<P><B>For ISBN: " . $isbnList[ $i ] . "</B>";
		}

		$CashOfferListTxt[ $i ] = $Title[ $i ];
		if ( $Edition[ $i ] != "" ) {
			$CashOfferListTxt[ $i ] .= "\nEdition: " . $Edition[ $i ];
		}
		if ( $Author[ $i ] != "" ) {
			$CashOfferListTxt[ $i ] .= "\n" . $Author[ $i ];
		}

		if ( ( $SalesRank[ $i ] > 1000000 ) || ( $SalesRank[ $i ] == "" ) ) {
			$CashOfferListGood[ $i ]   = 0;
			$CashOfferListAccept[ $i ] = 0;
			continue;
		}


		//Column C, After Fees, in ssample sheet1 table1
		//=(D3*0.85)-5
		$CashOffer = 0;
		if ( $LowesetUsed[ $i ] > 6 ) {
			$CashOffer = ( $LowesetUsed[ $i ] * 0.85 ) - 5;
		}
		if ( $debug ) {
			print "<P>(LowesetUsed of " . $LowesetUsed[ $i ] . "*0.85)-5=" . $CashOffer . ", (0 if Lowest<6).";
		}

		/*table lookup
		After Fee Price	-After Fee Price	-Amount subtracted	-Upper Margin %	-Lower Margin %
		-5	-0.01	0	0%	0%
		0	5.24	0	#DIV/0!	0%
		5.25	9.9900	3	133%	43%
		10	14.99	4	67%	36%
		15	19.99	5	50%	33%
		20	25.99	8	67%	44%
		26	35.99	10	63%	38%
		36	45.99	15	71%	48%
		46	55.99	20	77%	56%
		56	65.99	23	70%	54%
		70	79.99	25	56%	45%
		80	89.99	26	48%	41%
		90	99.99	30	50%	43%
		100	109.99	36	56%	49%
		110	119.99	40	57%	50%
		120	129.99	45	60%	53%
		130	139.99	50	63%	56%
		140	149.99	55	65%	58%
		150	159.99	60	67%	60%
		160	169.99	65	68%	62%
		170	179.99	70	70%	64%
		180	189.99	75	71%	65%
		190	199.99	80	73%	67%

		Column B, Cash Offer, in sample sheet1 table1
		=C2-VLOOKUP(C2,'Sheet 1 - Table 2'!$B$2:$E$12,4,TRUE)
		$CashOffer above - (lookup cash offer in the table to determine how much to subtract), that table is the if else block below
		//*/


		//After Fee table implemented below.  see comments a few lines down on how to change the table
		if ( $CashOffer <= 0 ) {
			//do nothing, it is already 0 and won't be bought
		} elseif ( $CashOffer < 5.24 ) {
			//do nothing, the offer isn't changed
		} elseif ( $CashOffer < 9.99 ) {
			$CashOffer = ( $CashOffer - 3 );


			/*to change the table, add/delete/change the lines in groups of 2 like below
			the 14.99 is the upper range on each line in the table above.  the lower range is not need for each block because it was handled in the previous elseif
			the amount to subtract, 4, is listed on the second line
			that's all there is to it.  :)
			//*/
			/*The Following is the Original Table, commented out so that there would be record of what the original looked like.  4-18-14
				}elseif ($CashOffer < 14.99) {
					$CashOffer = ($CashOffer - 4);


				}elseif ($CashOffer < 19.99) {
					$CashOffer = ($CashOffer - 5);
				}elseif ($CashOffer < 25.99) {
					$CashOffer = ($CashOffer - 8);
				}elseif ($CashOffer < 35.99) {
					$CashOffer = ($CashOffer - 10);
				}elseif ($CashOffer < 45.99) {
					$CashOffer = ($CashOffer - 15);
				}elseif ($CashOffer < 55.99) {
					$CashOffer = ($CashOffer - 20);
				}elseif ($CashOffer < 65.99) {
					$CashOffer = ($CashOffer - 23);
				}elseif ($CashOffer < 79.99) {
					$CashOffer = ($CashOffer - 25);
				}elseif ($CashOffer < 89.99) {
					$CashOffer = ($CashOffer - 26);
				}elseif ($CashOffer < 99.99) {
					$CashOffer = ($CashOffer - 30);
				}elseif ($CashOffer < 109.99) {
					$CashOffer = ($CashOffer - 36);
				}elseif ($CashOffer < 119.99) {
					$CashOffer = ($CashOffer - 40);
				}elseif ($CashOffer < 129.99) {
					$CashOffer = ($CashOffer - 45);
				}elseif ($CashOffer < 139.99) {
					$CashOffer = ($CashOffer - 50);
				}elseif ($CashOffer < 149.99) {
					$CashOffer = ($CashOffer - 55);
				}elseif ($CashOffer < 159.99) {
					$CashOffer = ($CashOffer - 60);
				}elseif ($CashOffer < 169.99) {
					$CashOffer = ($CashOffer - 65);
				}elseif ($CashOffer < 179.99) {
					$CashOffer = ($CashOffer - 70);
				}elseif ($CashOffer < 189.99) {
					$CashOffer = ($CashOffer - 75);
				}elseif ($CashOffer < 199.99) {
					$CashOffer = ($CashOffer - 80);
				}
				$CashOfferAccept = ($CashOffer * .80);
				if ($debug) { print "<P>After Fee Table Good offer is now " . $CashOffer . "."; }
				if ($debug) { print "<P>After Fee Table Acceptable offer is now " . $CashOfferAccept . "."; }

	//*/
		} elseif ( $CashOffer < 14.99 ) {
			$CashOffer = ( $CashOffer - 4 );


		} elseif ( $CashOffer < 19.99 ) {
			$CashOffer = ( $CashOffer - 5 );
		} elseif ( $CashOffer < 25.99 ) {
			$CashOffer = ( $CashOffer - 7 );
		} elseif ( $CashOffer < 35.99 ) {
			$CashOffer = ( $CashOffer - 8 );
		} elseif ( $CashOffer < 45.99 ) {
			$CashOffer = ( $CashOffer - 10 );
		} elseif ( $CashOffer < 55.99 ) {
			$CashOffer = ( $CashOffer - 14 );
		} elseif ( $CashOffer < 65.99 ) {
			$CashOffer = ( $CashOffer - 15 );
		} elseif ( $CashOffer < 79.99 ) {
			$CashOffer = ( $CashOffer - 20 );
		} elseif ( $CashOffer < 89.99 ) {
			$CashOffer = ( $CashOffer - 25 );
		} elseif ( $CashOffer < 99.99 ) {
			$CashOffer = ( $CashOffer - 28 );
		} elseif ( $CashOffer < 109.99 ) {
			$CashOffer = ( $CashOffer - 32 );
		} elseif ( $CashOffer < 119.99 ) {
			$CashOffer = ( $CashOffer - 35 );
		} elseif ( $CashOffer < 129.99 ) {
			$CashOffer = ( $CashOffer - 40 );
		} elseif ( $CashOffer < 139.99 ) {
			$CashOffer = ( $CashOffer - 50 );
		} elseif ( $CashOffer < 149.99 ) {
			$CashOffer = ( $CashOffer - 55 );
		} elseif ( $CashOffer < 159.99 ) {
			$CashOffer = ( $CashOffer - 60 );
		} elseif ( $CashOffer < 169.99 ) {
			$CashOffer = ( $CashOffer - 65 );
		} elseif ( $CashOffer < 179.99 ) {
			$CashOffer = ( $CashOffer - 70 );
		} elseif ( $CashOffer < 189.99 ) {
			$CashOffer = ( $CashOffer - 75 );
		} elseif ( $CashOffer < 199.99 ) {
			$CashOffer = ( $CashOffer - 80 );
		}
		$CashOfferAccept = ( $CashOffer * .80 );
		if ( $debug ) {
			print "<P>After Fee Table Good offer is now " . $CashOffer . ".";
		}
		if ( $debug ) {
			print "<P>After Fee Table Acceptable offer is now " . $CashOfferAccept . ".";
		}


		/* Sales Rank percentage reduction and max offer limit table implemented below
		Sales Rank Lower	Sales Rank Upper	Percentage used to decrease offer		Maximum Offer Price
		2						74,999				1										120
		75,000					124,999				0.9										120
		125,000	174,999	0.88		120
		175,000	224,999	0.85		60
		225,000	274,999	0.8		55
		275,000	324,999	0.8		50
		325,000	399,999	0.75		45
		400,000	599,999	0.7		40
		600,000	749,000	0.65		35
		750,000	999,999	0.5		30
		1,000,000	999,999,999			0.25
		//*/

		//Sales Rank percentage reduction and max offer limit table implemented below.  see comments a few lines down on how to change the table
		if ( $SalesRank[ $i ] < 74999 ) {
			//percentage is 100% so no change, just check max offer
			$MaxOffer     = 120;
			$SalesRankPer = ( 1.0 );
		} elseif ( $SalesRank[ $i ] < 124999 ) {
			$MaxOffer     = 120;
			$SalesRankPer = ( 0.9 );


			/*to change the table, add/delete/change the lines in groups of 5 like below
			the 174999 is the upper range on each line in the table above.  the lower range is not need for each block because it was handled in the previous elseif
			the percentage amount to multiply by, 0.88, is listed on the second line
			the maximum offer price is listed on the third and fourth lines and must be changed in both places
			that's all there is to it.  :)
			//*/
		} elseif ( $SalesRank[ $i ] < 174999 ) {
			$MaxOffer     = 120;
			$SalesRankPer = ( 0.88 );


		} elseif ( $SalesRank[ $i ] < 224999 ) {
			$MaxOffer     = 65;
			$SalesRankPer = ( 0.85 );
		} elseif ( $SalesRank[ $i ] < 274999 ) {
			$MaxOffer     = 60;
			$SalesRankPer = ( 0.84 );
		} elseif ( $SalesRank[ $i ] < 324999 ) {
			$MaxOffer     = 55;
			$SalesRankPer = ( 0.84 );
		} elseif ( $SalesRank[ $i ] < 399999 ) {
			$MaxOffer     = 50;
			$SalesRankPer = ( 0.8 );
		} elseif ( $SalesRank[ $i ] < 599999 ) {
			$MaxOffer     = 45;
			$SalesRankPer = ( 0.7 );
		} elseif ( $SalesRank[ $i ] < 749000 ) {
			$MaxOffer     = 44;
			$SalesRankPer = ( 0.65 );
		} elseif ( $SalesRank[ $i ] < 999999 ) {
			$MaxOffer     = 30;
			$SalesRankPer = ( 0.5 );
		}
		$CashOffer       = ( $CashOffer * $SalesRankPer );
		$CashOfferAccept = ( $CashOfferAccept * $SalesRankPer );
		if ( $debug ) {
			print "<P>Sales Rank % reduction, Good offer is now " . $CashOffer . ".";
		}
		if ( $debug ) {
			print "<P>Sales Rank % reduction, Accept offer is now " . $CashOfferAccept . ".";
		}


		//round to nearest 25 cents, In excel, =ROUND(A1/.25,0)*.25,  The formula divides the original value by .25 and then multiplies the result by .25. You can, of course, use a similar formula to round values to other fractions. For example, to round a dollar amount to the nearest nickel, simply substitute .05 for each of the two occurrences of ".25" in the preceding formula.
		$CashOffer       = round( $CashOffer, 0, PHP_ROUND_HALF_UP );
		$CashOfferAccept = round( $CashOfferAccept, 0, PHP_ROUND_HALF_UP );
		if ( $debug ) {
			print "<P>Round to nearest .25 cents, Good offer is now " . $CashOffer . ".";
		}
		if ( $debug ) {
			print "<P>Round to nearest .25 cents, Accept offer is now " . $CashOfferAccept . ".";
		}


		//Take an additional %15 off the Good condition price and choose the lesser of that or the $MaxOffer price in the Sales Rank table above
		//$CashOffer85 = $CashOffer * 0.85;
		//if ($debug) { print "<P>Good condition - 15% offer is now " . $CashOffer85 . "."; }
		//if ($CashOffer85 > $MaxOffer) {
		if ( $debug ) {
			print "<P>The lesser of Good $CashOffer and $MaxOffer is ";
		}
		if ( $CashOffer > $MaxOffer ) {
			$CashOffer = $MaxOffer;
			/*
			}else{
				$CashOffer = $CashOffer85;
			//*/
		}
		if ( $debug ) {
			print " $CashOffer.";
		}


		if ( $debug ) {
			print "<P>The lesser of Accept $CashOfferAccept and $MaxOffer is ";
		}
		if ( $CashOfferAccept > $MaxOffer ) {
			$CashOfferAccept = $MaxOffer;
		}
		if ( $debug ) {
			print " $CashOfferAccept.";
		}


		if ( $CashOffer <= 0 ) {
			$CashOfferListGood[ $i ]   = 0;
			$CashOfferListAccept[ $i ] = 0;
		} elseif ( $CashOffer > 199.99 ) {  //this should never execute because of sales rank table above, but left it in anyway
			$CashOfferListGood[ $i ]   = 199.99;
			$CashOfferListAccept[ $i ] = 199.99;
		} else {
			$CashOfferListGood[ $i ]   = $CashOffer;
			$CashOfferListAccept[ $i ] = $CashOfferAccept;
			$OfferTotalGood += $CashOffer;
			$OfferTotalAccept += $CashOfferAccept;
		}
	} //for ($i=0; $i<sizeof($isbnList); $i++) {	//compute offer and display on the screen


	//after all the books have been processed, if total offer > 10, go back through all the offers and change the 0s to .25 and add that back in total.
	//Edited error message to "invalid" 4/22/14
	if ( $OfferTotalGood > 10 ) {
		for ( $i = 0; $i < sizeof( $CashOfferListGood ); $i ++ ) {
			if ( ( $CashOfferListGood[ $i ] == 0 ) && ( stripos( $Title[ $i ], "invalid" ) === false ) ) {
				$CashOfferListGood[ $i ] = 1.00;
				$OfferTotalGood += 1.00;
			}
		}
	}

	if ( $OfferTotalAccept > 10 ) {
		for ( $i = 0; $i < sizeof( $CashOfferListAccept ); $i ++ ) {
			if ( ( $CashOfferListAccept[ $i ] == 0 ) && ( stripos( $Title[ $i ], "invalid" ) === false ) ) {
				$CashOfferListAccept[ $i ] = 0.75;
				$OfferTotalAccept += 0.75;
			}
		}
	}


	$OfferText = "";
	for ( $i = 0; $i < sizeof( $isbnList ); $i ++ ) {
		$OfferText .= $isbnList[ $i ] . ' - ' . $CashOfferListTxt[ $i ] . '\nGood: $' . number_format( $CashOfferListGood[ $i ], 2 ) . ', Accept: $' . number_format( $CashOfferListAccept[ $i ], 2 ) . "\n\n";
	}
	$OfferText .= '--- Total Good: $' . number_format( $OfferTotalGood, 2 ) . ',   Accept: $' . number_format( $OfferTotalAccept, 2 ) . "\n";


	//save data to news db.
	/* Original changed 4/20/14

$sqls="INSERT INTO OffersLog (TimeStamp, OfferText, OfferTotalGood, OfferTotalAccept, LookupCount, ReturnedCount, XmlOutput) values (\"" . date("Y-m-d H:i:s") . "\", \"" . str_replace('"', '\"', $OfferText) . "\", " . $OfferTotalGood . ", " . $OfferTotalAccept . ", " . sizeof($isbnList) . ", " . $ReturnedCount . ", \"" . str_replace('"', '\"', $XmlOutput) . "\" )";
//*/


	//NEW fixed timestamp in DB
//		$sqls = "INSERT INTO {$offer_log->table_name} (TimeStamp, OfferText, OfferTotalGood, OfferTotalAccept, LookupCount, ReturnedCount, XmlOutput)
//				values (\"" . date( "Y-m-d H:i:s", mktime( ( date( "H" ) - 4 ), date( "i" ), date( "s" ), date( "m" ), date( "d" ), date( "Y" ) ) ) . "\", \"" . str_replace( '"', '\"', $OfferText ) . "\", " . $OfferTotalGood . ", " . $OfferTotalAccept . ", " . sizeof( $isbnList ) . ", " . $ReturnedCount . ", \"" . str_replace( '"', '\"', $XmlOutput ) . "\" )";
	$recordid = 0;
	if ( ! empty( $last_id ) ) {
		$updated_rows = $offer_log->update( array(
			'TimeStamp'        => date( "Y-m-d H:i:s", mktime( ( date( "H" ) - 4 ), date( "i" ), date( "s" ), date( "m" ), date( "d" ), date( "Y" ) ) ),
			'OfferText'        => str_replace( '"', '\"', $OfferText ),
			'OfferTotalGood'   => $OfferTotalGood,
			'OfferTotalAccept' => $OfferTotalAccept,
			'LookupCount'      => sizeof( $isbnList ),
			'ReturnedCount'    => $ReturnedCount,
			'XmlOutput'        => str_replace( '"', '\"', $XmlOutput )
		), array( 'ID' => $last_id ) );
		if ( $updated_rows ) {
			$recordid = $last_id;
		}

	} else {
		$recordid = $offer_log->insert( array(
			'TimeStamp'        => date( "Y-m-d H:i:s", mktime( ( date( "H" ) - 4 ), date( "i" ), date( "s" ), date( "m" ), date( "d" ), date( "Y" ) ) ),
			'OfferText'        => str_replace( '"', '\"', $OfferText ),
			'OfferTotalGood'   => $OfferTotalGood,
			'OfferTotalAccept' => $OfferTotalAccept,
			'LookupCount'      => sizeof( $isbnList ),
			'ReturnedCount'    => $ReturnedCount,
			'XmlOutput'        => str_replace( '"', '\"', $XmlOutput )
		) );
	}


	//$resins = mysql_query( $sqls );
	if ( $debug ) {
		print "<p>resins=" . $resins . " sqls=" . $sqls;
	}

	/*if ( mysql_affected_rows() == 1 ) {
		$recordid = mysql_insert_id();
		if ( $debug ) {
			print "<BR>Added successfully as record ID: $recordid.";
		}
	} else {
		echo "<p><b>ERROR adding.</b> ", mysql_error();
	}*/

	return array(
		'CashOfferListTxt'    => $CashOfferListTxt,
		'Image'               => $Image,
		'Timestamp'           => $Timestamp,
		'OfferTotalGood'      => $OfferTotalGood,
		'OfferTotalAccept'    => $OfferTotalAccept,
		'CashOfferListGood'   => $CashOfferListGood,
		'CashOfferListAccept' => $CashOfferListAccept,
		'recordid'            => $recordid,
	);

}

add_shortcode( 'sell_books', 'interface_sell_books_shortcode' );

//add_action( 'init', 'sell_books_form_callback' );

function sell_books_form_callback() {

	global $wpdb;
	$theme_path_buypack = get_stylesheet_directory_uri();
	wp_enqueue_style('dashicons');
	wp_enqueue_script( 'books-enqueue', $theme_path_buypack . '/js/main.js', array( 'jquery' ));
	wp_enqueue_script( 'jquery-colorbox', $theme_path_buypack . '/js/jquery.colorbox-min.js' );
	wp_enqueue_style( 'jquery-colorbox', $theme_path_buypack . '/css/colorbox.css' );
	$offer_log     = new Offer_Log();
	$offer_log_fau = new Offer_Log_FAU();
	$offer_log_na  = new Offer_Log_NA();
	if ( isset( $_POST['GetOffer'] ) ) {
		if ( empty( $_POST['isbns'] ) ) {
			$error          = __( 'Please enter ISBN codes separated by comma.' );
			$_POST['error'] = $error;

			return;
		}

		$isbns = $_POST['isbns'];

		if ( isset( $_POST['book_isbns'] ) ) {
			$org_isbns = $_POST['book_isbns'];
			$org_isbns = trim( $org_isbns );
			$isbns .= ',' . $org_isbns;
		}

		$isbnList = array();
		$isbns    = trim( $isbns );
		$isbns = str_replace( "\n", ',', $isbns );
		$isbnList = explode( ",", $isbns );

		$isbnList = array_map('trim',$isbnList);

		$isbnList = array_filter( $isbnList );
		$isbnList = str_replace('-','',$isbnList);
		$isbns    = implode( ',', $isbnList );

		if ( isset( $_POST['recordid'] ) ) {
			$x = get_book_details_amazon( $isbnList, $_POST['recordid'] );
		} else {
			$x = get_book_details_amazon( $isbnList );
		}


		//display offers
		ob_start();
		/*print '
			<center>
			<TABLE cellspacing="5" cellpadding="5">
			<TR>
				<TD align="center"><B>Our cash buyback price</B></TD>
				<TD><B>Book Photo</B></TD>
				<TD>Delete book</TD>
			</TR>
		';*/

		?>
		<table cellspacing="5" cellpadding="5">
			<tr>
				<td align="center">
					<b>Our cash buyback price</b>
				</td>
				<td>
					<b>Book Photo</b>
				</td>
				<td>Delete Book</td>
			</tr>
		<?php

		//changed offer wording 4/22/14
		for ( $i = 0; $i < sizeof( $isbnList ); $i ++ ) {
			/*print '
				<TR data-isbn="' . $isbnList[ $i ] . '" data-accept="' . number_format( $x['CashOfferListAccept'][ $i ], 2 ) . '" data-good="' . number_format( $x['CashOfferListGood'][ $i ], 2 ) . '">
					<TD valign="top">' . $isbnList[ $i ] . '<br>' . nl2br( $x['CashOfferListTxt'][ $i ] ) . '<br>Good Condition: $' . number_format( $x['CashOfferListGood'][ $i ], 2 ) . ', Acceptable Condition: $' . number_format( $x['CashOfferListAccept'][ $i ], 2 ) . '
					</TD>
					<TD valign="top"><IMG SRC="' . $x['Image'][ $i ] . '" BORDER=0 ALT=""></TD>
					<TD><a href="#" class="book-delete"><span class="dashicons dashicons-trash"></span></a></TD>
				</TR>
			';*/
			?>
			<tr data-isbn="<?php echo $isbnList[ $i ]; ?>"
			    data-accept="<?php echo number_format( $x['CashOfferListAccept'][ $i ], 2 ); ?>"
				data-good="<?php echo number_format( $x['CashOfferListGood'][ $i ], 2 ); ?>">
				<td valign="top">
					<?php echo $isbnList[ $i ]; ?>
					<br/>
					<?php echo nl2br( $x['CashOfferListTxt'][ $i ] ); ?>
					<br/>
					Good Condition: &dollar;<?php echo number_format( $x['CashOfferListGood'][ $i ], 2 ); ?>
					<br/>
					Acceptable Condition: &dollar;<?php echo number_format( $x['CashOfferListAccept'][ $i ], 2 ); ?>
				</td>
				<td valign="top">
					<img src="<?php echo $x['Image'][ $i ]; ?>" border="0" alt="<?php echo $x['Image'][ $i ]; ?>" />
				</td>
				<td>
					<a href="#" class="book-delete"><span class="dashicons dashicons-trash"></span></a>
				</td>
			</tr>
			<?php
		}

		?>
		<tr class="top">
			<td colspan="3">
				Total for Good Condition: &dollar;<span id="book-offer-good"><?php echo number_format( $x['OfferTotalGood'], 2 ); ?></span>
				<br>
				Total for Acceptable Condition: &dollar;<span id="offer-accept-book"><?php echo number_format( $x['OfferTotalAccept'], 2 ); ?></span>
			</td>
		</tr>
		</table>
		<p align="left"><strong>Click "Sell Books" and fill out form to arrange pickup.</strong></p>
		<form id="form1" name="form1" action="<?php echo get_permalink(); ?>" method="post">
			<table>
				<tr>
					<td>
						<input type="hidden" name="recordid" value="<?php echo $x['recordid']; ?>">
						<input type="hidden" id="book-isbns" name="isbn" value="<?php echo $isbns; ?>">
						<input type="hidden" name="org_isbn" value="<?php echo $isbns; ?>">
						<button name="SellBooks" class="btn btn-success btn-large" value="Sell">Sell Books</button>
						<button name="AddMoreBooks" id="add-more-books" class="btn btn-success btn-large" value="Add More Books">Add More Books</button>
					</td>
				</tr>
			</table>
		</form>
		<div style="display: none;">
		<form id="price-books-form" name="price-books-form" action="<?php echo get_permalink(); ?>"
		      method="post">

			<?php if ( isset( $_POST['error'] ) ) {
				?><span class="error"><?php echo $_POST['error']; ?></span><?php
			} ?>

			<textarea class="txt_isbns" required
			          placeholder="Click to enter ISBNs Example: 9783593812489,9784930948173,9784829577392,9783840948274"
			          name="isbns" row="15" class="input-block-level"></textarea>

			<input class="buy_GetOffer" type="submit" name="GetOffer" value="Get More Prices »">
			<input type="hidden" name="recordid" value="<?php echo $x['recordid']; ?>">
			<input type="hidden" id="book-isbns-add-more" name="book_isbns" value="<?php echo $isbns; ?>">
		</form>
		</div>

		<?php

		/*print '

			<tr class="top">
				<TD colspan="3">Total for Good Condition: $<span id="book-offer-good">' . number_format( $x['OfferTotalGood'], 2 ) . '</span><br>  Total for Acceptable Condition: $<span id="offer-accept-book">' . number_format( $x['OfferTotalAccept'], 2 ) . '</span></TD>


		';*/

		/*print '
			</TABLE>
	<!--EDITED Removed spacing gap on 4/20/14 <br>&nbsp; -->

<p align="left"><strong>Click "Sell Books" and fill out form to arrange pickup.</strong></p>
			<form id="form1" name="form1" action="' . get_permalink() . '" method="post">
			<TABLE>
			<TR>
				<TD>
				<input type="hidden" name="recordid" value="' . $x['recordid'] . '">
				<input type="hidden" id="book-isbns" name="isbn" value="' . $isbns . '">
				<input type="hidden" name="org_isbn" value="' . $isbns . '">
				<button name="SellBooks" class="btn btn-success btn-large" value="Sell">Sell Books</button>
				<button name="AddMoreBooks" class="btn btn-success btn-large" value="Add More Books">Add More Books</button>
				</TD>
			</TR>
			</TABLE>
			</form>

			</center>

			</BODY>
			</HTML>

		';*/

		echo ob_get_clean();

		return true;
	} elseif ( isset( $_POST['SellBooks'] ) ) {
		$recordid = $_POST['recordid'];
		if ( isset( $_POST['org_isbn'], $_POST['isbn'] ) && $_POST['org_isbn'] != $_POST['isbn'] ) {
			// few books removed maybe?!
			$isbns    = trim( $_POST['isbn'] );
			$isbnList = explode( ",", $isbns );
			$x        = get_book_details_amazon( $isbnList, $recordid );
		}
		?>
		<center>
			<P>We will get in touch with you within 24 hours and arrange a time and place to meet up for the textbooks.
				<br>
				<BR><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>Required field.
			<p align="center"><B><FONT face="Arial" COLOR="ff0000" size="2"><?php print $verrormsg; ?></FONT></B></p>
			<form id="form1" name="form1" action="<?php echo get_permalink(); ?>" method="post">

				<TABLE>
					<TR>
						<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>First and Last Name:
						</TD>
						<TD><input maxLength="25" type="text" name="Name" placeholder="First name Last name" size="25"
						           value="<?php print $Name; ?>"></TD>
					</TR>

					<TR>
						<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>Email:</TD>
						<TD><input maxLength="50" type="email" name="Email" placeholder="Email" size="25"
						           value="<?php print $Email; ?>"></TD>
					</TR>

					<TR>
						<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>Cell phone
							number<BR>(so we can arrange meet up):
						</TD>
						<TD><input maxLength="15" type="tel" name="Cell" placeholder="Phone #" size="15"
						           value="<?php print $Cell; ?>"></TD>
					</TR>

					<TR>
						<TD align="right">How did you hear about us?:</TD>
						<TD>
							<select name="ReferredBy">
								<option value="" <?php if ( $ReferredBy == "" ) {
									print " selected ";
								} ?> >Select
								</option>
								<option value="Facebook" <?php if ( $ReferredBy == "Facebook" ) {
									print " selected ";
								} ?> >Facebook
								</option>
								<option value="Friend" <?php if ( $ReferredBy == "Friend" ) {
									print " selected ";
								} ?> >From a friend
								</option>
								<option value="Flyer" <?php if ( $ReferredBy == "Flyer" ) {
									print " selected ";
								} ?> >Flyer/Poster
								</option>
								<option value="GreekLife" <?php if ( $ReferredBy == "GreekLife" ) {
									print " selected ";
								} ?> >Greek Life Fraternity/Sorority
								</option>
								<option value="Email" <?php if ( $ReferredBy == "Email" ) {
									print " selected ";
								} ?> >Received email
								</option>
								<option value="Other" <?php if ( $ReferredBy == "Other" ) {
									print " selected ";
								} ?> >Other
								</option>
							</select>
						</TD>
					</TR>

					<!--<TR>
						<!--	<TD align="right">Prefer to meet:</TD>-->
					<!--	<TD>-->
					<!--<input type="radio" name="Meet" value="OnCampus" <?php if ( $Meet == "OnCampus" ) {
						print " checked ";
					} ?> >On-Campus<br>-->
					<!--<input type="radio" name="Meet" value="OffCampus" <?php if ( $Meet == "OffCampus" ) {
						print " checked ";
					} ?> >Off-Campus-->
					<!-- </TD>-->
					<!-- </TR> -->
					<TR>
						<TD>&nbsp;</TD>
						<TD>
							<input type="hidden" name="recordid" value="<?php print $recordid; ?>">
							<button name="ContactMe" class="classname1" value="Sell">Submit Books</button>
						</TD>
					</TR>
				</TABLE>

			</form>
		</center>
		<?php
		return true;
	} elseif ( isset( $_POST['ContactMe'] ) ) {
		$verrormsg  = "";
		$Name       = $_POST['Name'];
		$Email      = $_POST['Email'];
		$Cell       = $_POST['Cell'];
		$ReferredBy = $_POST['ReferredBy'];
		$Meet       = $_POST['Meet'];
		$recordid   = $_POST['recordid'];
		if ( ( $Name == "" ) || ( $Email == "" ) || ( $Cell == "" ) ) {
			$verrormsg = "Missing: ";

			if ( $Name == "" ) {
				$verrormsg .= "Name, ";
			}

			if ( $Email == "" ) {
				$verrormsg .= "Email, ";
			}

			if ( $Cell == "" ) {
				$verrormsg .= "Cell, ";
			}

			//remove last ", "
			$verrormsg = substr( $verrormsg, 0, strlen( $verrormsg ) - 2 ) . ".";
			?>


			<center>

				<P>Please enter your contact info and Press the Contact Me button.
					<BR>Fields with <B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B> are required.

				<p align="center"><B><FONT face="Arial" COLOR="ff0000" size="2"><?php print $verrormsg; ?></FONT></B>
				</p>

				<form id="form1" name="form1" action="<?php echo get_permalink(); ?>" method="post">
					<TABLE>
						<TR>
							<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>First and Last
								Name:
							</TD>
							<TD><input type="text" maxLength="25" name="Name" placeholder="First name Last name"
							           size="25"
							           value="<?php print $Name; ?>"></TD>
						</TR>

						<TR>
							<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>Email:</TD>
							<TD><input maxLength="50" type="email" name="Email" placeholder="Email" size="25"
							           value="<?php print $Email; ?>"></TD>
						</TR>

						<TR>
							<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>Cell phone
								number<BR>(so we can arrange meet up):
							</TD>
							<TD><input maxLength="15" type="tel" name="Cell" placeholder="Phone #" size="15"
							           value="<?php print $Cell; ?>"></TD>
						</TR>

						<TR>
							<TD align="right">How did you hear about us?:</TD>
							<TD>
								<select name="ReferredBy">
									<option value="" <?php if ( $ReferredBy == "" ) {
										print " selected ";
									} ?> >Select
									</option>
									<option value="Facebook" <?php if ( $ReferredBy == "Facebook" ) {
										print " selected ";
									} ?> >Facebook
									</option>
									<option value="Friend" <?php if ( $ReferredBy == "Friend" ) {
										print " selected ";
									} ?> >From a friend
									</option>
									<option value="Flyer" <?php if ( $ReferredBy == "Flyer" ) {
										print " selected ";
									} ?> >Flyer/Poster
									</option>
									<option value="FratSor" <?php if ( $ReferredBy == "FratSor" ) {
										print " selected ";
									} ?> >Greek Life Fraternity/Sorority
									</option>
									<option value="Email" <?php if ( $ReferredBy == "Email" ) {
										print " selected ";
									} ?> >Received email
									</option>
									<option value="Other" <?php if ( $ReferredBy == "Other" ) {
										print " selected ";
									} ?> >Other
									</option>
								</select>
							</TD>
						</TR>

						<TR>
							<TD align="right">Prefer to meet:</TD>
							<TD>
								<input type="radio" name="Meet" value="OnCampus" <?php if ( $Meet == "OnCampus" ) {
									print " checked ";
								} ?> >On-Campus<br>
								<input type="radio" name="Meet" value="OffCampus" <?php if ( $Meet == "OffCampus" ) {
									print " checked ";
								} ?> >Off-Campus
							</TD>
						</TR>

						<TR>
							<TD>&nbsp;</TD>
							<TD>&nbsp;</TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
							<TD>
								<input type="hidden" name="recordid" value="<?php print $recordid; ?>">
								<button name="ContactMe" class="classname1" value="Submit Books">Submit Books</button>
							</TD>
						</TR>
					</TABLE>

				</form>

			</center>


			<?php
		} else {
			$offer_log->update( array(
				'Name'       => $Name,
				'Email'      => $Email,
				'Cell'       => $Cell,
				'ReferredBy' => $ReferredBy,
				'Meet'       => $Meet,
			), array( 'ID' => $recordid ) );

//			$sqls   = "UPDATE OffersLog set Name=\"" . $Name . "\", Email=\"" . $Email . "\", Cell=\"" . $Cell . "\", ReferredBy=\"" . $ReferredBy . "\", Meet=\"" . $Meet . "\" where ID=" . $recordid;
//			$result = mysql_query( $sqls );
			//print ("res=" . $result . " sqls=" . $sqls);
//			if ( mysql_affected_rows() >= 0 ) {
			//print ("<BR><P>" . mysql_affected_rows() . " record(s) updated for " . $table2edit . " number " . $recordid . ".");
//			} else {
//				echo "<P>Error, UPDATE failed: ", mysql_error();
			// exit; to where? todo
//			}


			$sqls = $wpdb->prepare( "select OfferText from {$offer_log->table_name} where ID=%d", $recordid );
			$row  = $wpdb->get_row( $sqls, ARRAY_A );
//			$result = mysql_query( $sqls );
			//print ("res=" . $result . " sqls=" . $sqls);
//			$row = mysql_fetch_array( $result );

			/*
			5) This form should then be sent with all the information that they filled out, as well as a list of the ISBN, Title, and Price for each book, the totals, and breakdown of the multi book bonus and total for that as well.
			//*/

			$msg = "Name: $Name\nEmail: $Email\nCell:$Cell\nReferredBy:$ReferredBy\nMeet:$Meet\nOffer Details:\n" . $row["OfferText"] . "Record ID:$recordid\n" . "\n\nEnd of email";
			//mail("whetzels1@gmail.com", "Cash For Books Request from 365buyback .com", $msg, "From: " . $Email . "\nReply-To: " . $Email, "-F$vEmail");

			$To = "sales@360buyback.com,mehul.kaklotar@gmail.com";

			wp_mail( $To, "Customer Submission", $msg, "From: " . $Email . "\nReply-To: " . $Email );

			$msg = "Hello $Name,\n\nThank you for submitting your books at 360BuyBack.com and supporting a 100% student owned and run business.\n\nPlease verify that $Cell is your correct number, you should be receiving a text from us shortly to arrange a convenient time to meet up for your books. \n\n" . "Here is the information we recieved from you:\nName: $Name\nEmail: $Email\nCell:$Cell\nReferredBy:$ReferredBy\nMeet:$Meet\nOffer Details:\n" . $row["OfferText"] . "\n\nSincerely,\n 360 Buyback";
			//mail("whetzels1@gmail.com", "Cash For Books Request from 365buyback .com", $msg, "From: " . $Email . "\nReply-To: " . $Email, "-F$vEmail");


			$To = "$Email";

			wp_mail( $To, "Books Submitted - 360BuyBack!", $msg, "From: " . "books@360buyback.com" . "\nReply-To: " . $Email );

			//display offers
			print '
		<BODY>

		<center>

		<P>Thank you for giving us the opportunity to purchase your books!
		<p> We are a local company 100% owned and run by college students.  We will contact you within 24 hours or less.
		If you do not recieve an automated email from us immediately there may have been a problem because of 1 or 2 reasons...
		<p>1) The email address you provided was invalid
		<p>2) Something went wrong with our program (beta version)
		<p>Please shoot us an email <a href="mailto:service@360buyback.com?subject=Possible Error">service@360BuyBack.com</a> and we will personally run your books through the system manually.

		<p>Thanks again for using 360BuyBack.com  Be sure to tell your friends about us!</p>

		<a href="http://facebook.com/365buyback"><img src="http://www.runaroundtech.com/wp-content/uploads/2012/09/like_us_on_facebook1.jpg" alt="Like us on Facebook" width="140" height="60"></a> for an <strong> <font size="3.65">extra</strong></font> 5% on your order. </p>

		</center>

		</BODY>
		</HTML>
		';


		}

		return true;
	}

	return false;

}

/**************************************************************************************/
?>
