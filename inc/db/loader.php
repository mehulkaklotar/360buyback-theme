<?php
/**
 * Created by PhpStorm.
 * User: spock
 * Date: 30/12/15
 * Time: 12:15 AM
 */

$theme_path_buypack = get_template_directory();

include_once $theme_path_buypack . '/inc/db/lib/rt-db-model/class-rt-db-model.php';
include_once $theme_path_buypack . '/inc/db/lib/rt-db-update/class-rt-db-update.php';
include_once $theme_path_buypack . '/inc/db/model/class-offer-log.php';
include_once $theme_path_buypack . '/inc/db/model/class-offer-log-fau.php';
include_once $theme_path_buypack . '/inc/db/model/class-offer-log-NA.php';


add_action( 'admin_init', 'buy_pack_database_update' );


function buy_pack_database_update(){
	$updateDB = new RT_DB_Update( false, trailingslashit( get_template_directory() . '/inc/db/schema' ) );
	$updateDB->do_upgrade();
}