<?php
/**
 * Don't load this file directly!
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'Offer_Log' ) ) {
	class Offer_Log extends RT_DB_Model {

		/**
		 * Offer_Log constructor.
		 */
		public function __construct() {
			parent::__construct( 'OffersLog' );
		}
	}
}