<?php
/**
 * Don't load this file directly!
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'Offer_Log_NA' ) ) {
	class Offer_Log_NA extends RT_DB_Model {

		/**
		 * Offer_Log_NA constructor.
		 */
		public function __construct() {
			parent::__construct( 'OffersLogNA' );
		}
	}
}