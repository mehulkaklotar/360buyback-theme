<?php
/**
 * Don't load this file directly!
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'Offer_Log_FAU' ) ) {
	class Offer_Log_FAU extends RT_DB_Model {

		/**
		 * Offer_Log_FAU constructor.
		 */
		public function __construct() {
			parent::__construct( 'OffersLogFAU' );
		}
	}
}