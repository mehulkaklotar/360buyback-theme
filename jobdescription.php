<html>
<body>
<?php
/*
Template Name: jobdescription
*/
/**
 * XClusive - template for displaying all pages
 *
 * @file page.php
 * @package xclusive 
 * @author Hafid Trujillo
 * @copyright 2013 Xpark Media
 * @license license.txt 
 * @version release: 0.1.0
 * @filesource  wp-content/themes/xclusive/page.php
 * @since available since 0.1.0
 */
 
  get_header(); ?>
  
		<b>Who are we?</b>

<p>360BuyBack is a textbook buyback company that was started by college students, for college students.  Our founder was tired of friends and classmates getting ripped off by the University Bookstore and not knowing what to do with the books that the bookstore was not buying back.</p>


<b>So what do we do?</b>

<p>360BuyBack hires you and other students to personally buy back books on your campus at the end of every semester. Students come to us for the higher prices and convenience that we offer.  We then collect all the books and send them to our distribution center, where they are sold on a national level to students nationwide!</p>

<b> </b>

<b>How does the buyback work?</b>

<p>Upon joining our team, you will be provided with a username and password of your choice, which gives you exclusive access to our "Sales Representative" online program page.  This program allows you to input the ISBNs of students' books.  The program will give you the price you pay the students for their books along with your commission for buying the books, all in a simple, easy to use interface. The student will then decide which books they want to sell. This is where you will meet up with the student and exchange the cash for the books. Your "Campus Manager" will collect the books you've bought, and we pay you your commission (and any potential bonuses you earn) on the total amount you buyback. It's that simple!</p>

&nbsp;

<p>Since 360BuyBack is only as good as its sales team, it is important for you to know exactly who we are looking for in a Sales Representative. We want people who are naturally outgoing and enthusiastic. You need to be comfortable advertising, approaching people you don't know in a friendly way. This is a fantastic opportunity to work on social skills for sales! Generally, the most successful buyers are those who have access to the dorms and are well connected on campus. You can make announcements at your fraternity and sorority chapter meetings or group pages, use your Facebook and Twitter, clubs, sports teams and word of mouth to spread the word that you buy back textbooks for cash!  All while saving them the hassle of waiting in line to deal with the bookstore. You may also receive leads directly from us, meaning we may provide you with students ready and willing to sell their books. They just need someone to meet up with them!</p>

<b> </b>

<b>This job is probably not for you if you: </b>

<p>You are overwhelmed with finals and other activities during the last 2 weeks of the semester.</p>

&nbsp;

<b>FAQ</b>:



<b>Why work for us vs. other buyback companies:</b>

<p>We pay the MOST for books, meaning you are getting a commission off of a higher dollar amount.  ALSO, we pay a much higher commission, an industry leading 15%!  That is 3x higher than our closest competitor!</p>

&nbsp;

<b>How is the commission calculated?</b>

<p>You are paid 15% of whatever the buyback price is.  For example, if your friend Bob wants to sell you his books, and we are offering to pay $90 for his books, you will receive: $90 x 0.15 = $13.5
<br>
<br>Or
<br>
<br>$100 x 0.15 = $15

You would make $13.50 if the books were worth $90, or $15 if they were worth $100.

That’s more than minimum wage, for one quick transaction!

As you can see, the possibilities for income are virtually limitless.</p>

&nbsp;

<b> </b>

<b>How do I get started?</b>

<p>Just press the “Apply” button, complete our very short application and wait to hear from us.  If we think you may be a good fit, we will arrange a time to meet with you or schedule a phone interview.  Once hired, we will explain the process and show you everything you need to know to start buying books and making money!</p>

&nbsp;

<b>Why will students want to sell to 360BuyBack instead of the University Bookstore?</b>
<p>
9/10 times, we pay MORE for their books AND we provide them the ease/convenience of selling their books.</p>

&nbsp;

<b>Can I buy from students, friends or family at other schools?</b>
<p>
Absolutely! You can buy from students at other schools. You can also buy from family and friends. You can buy from anyone that has books they want to sell. Any book can be bought from anyone. The more you buy, the more cash you earn in commissions and bonuses!</p>

<b> </b>

<b>Do we buy all books?</b>
<p>
Almost all.  We buy many books that the university bookstore will not buy. Generally, the only books we do not buy are International Editions, Teachers editions, and University Specific editions.</p>

<b> </b>

<b>Do I have to be a student to be a buyer?</b>
<p>
Because this position is based to a large degree upon social networking and the understanding of one's own campus, we only hire students.</p>

<b> </b>

<b>How much time do I need to spend buying books?</b>
<p>
Obviously we want you to spend as much time as you can without falling behind in your academics.  Ideally, you will spend multiple hours during the finals period at your school since this is the time when everyone is getting rid of their books.  The more you buy, the more you make. YOU determine how much you make!</p>

<b> </b>

<b>What do I do with the books I buy?</b>
<p>
You will be responsible for storing the books until a Campus Manager meets up with you to pick them up.  If you find yourself running out of space, simply get in touch with your Campus Manager to schedule a pickup.
</p>
<b> </b>

<b>How do I ship these books to you?</b> <p>You don’t need to ship the books. When you are done with the buyback, your Campus Manager will pick up the books.</p>

&nbsp;

<b>How do I get paid?</b><p> Each time your books are picked up, or at the end of the buyback period, you will earn a percentage of the total value of the books you bought, as well as any bonuses you might have earned. We use Paypal to get your money to you as fast as possible, but if you do not have Paypal, we can pay you by check. </p>
		
			<div id="content" role="main">
			
				
               
			
			
			
		</div><!--#content-->
	</section><!--#primary-->

 </body>
 </html>
