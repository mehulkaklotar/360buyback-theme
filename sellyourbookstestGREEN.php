<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style type="text/css">
.classname {
	-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	-webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #79bbff), color-stop(1, #378de5) );
	background:-moz-linear-gradient( center top, #79bbff 5%, #378de5 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#79bbff', endColorstr='#378de5');
	background-color:#79bbff;
	-webkit-border-top-left-radius:20px;
	-moz-border-radius-topleft:20px;
	border-top-left-radius:20px;
	-webkit-border-top-right-radius:20px;
	-moz-border-radius-topright:20px;
	border-top-right-radius:20px;
	-webkit-border-bottom-right-radius:20px;
	-moz-border-radius-bottomright:20px;
	border-bottom-right-radius:20px;
	-webkit-border-bottom-left-radius:20px;
	-moz-border-radius-bottomleft:20px;
	border-bottom-left-radius:20px;
	text-indent:0;
	border:1px solid #84bbf3;
	display:inline-block;
	color:#FFFFFF;
	font-family:Arial;
	font-size:22px;
	font-weight:bold;
	font-style:normal;
	height:39px;
	line-height:39px;
	width:138px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 0px #528ecc;
}
.classname:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #378de5), color-stop(1, #79bbff) );
	background:-moz-linear-gradient( center top, #378de5 5%, #79bbff 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff');
	background-color:#378de5;
}.classname:active {
	position:relative;
	top:1px;
}
.classname1 {
	-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	-webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #79bbff), color-stop(1, #378de5) );
	background:-moz-linear-gradient( center top, #79bbff 5%, #378de5 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#79bbff', endColorstr='#378de5');
	background-color:#79bbff;
	-webkit-border-top-left-radius:20px;
	-moz-border-radius-topleft:20px;
	border-top-left-radius:20px;
	-webkit-border-top-right-radius:20px;
	-moz-border-radius-topright:20px;
	border-top-right-radius:20px;
	-webkit-border-bottom-right-radius:20px;
	-moz-border-radius-bottomright:20px;
	border-bottom-right-radius:20px;
	-webkit-border-bottom-left-radius:20px;
	-moz-border-radius-bottomleft:20px;
	border-bottom-left-radius:20px;
	text-indent:0;
	border:1px solid #84bbf3;
	display:inline-block;
	color:#FFFFFF;
	font-family:Arial;
	font-size:22px;
	font-weight:bold;
	font-style:normal;
	height:39px;
	line-height:39px;
	width:175px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 0px #528ecc;
}
.classname:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #378de5), color-stop(1, #79bbff) );
	background:-moz-linear-gradient( center top, #378de5 5%, #79bbff 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff');
	background-color:#378de5;
}.classname:active {
	position:relative;
	top:1px;
}
</style>
<!-- Facebook Conversion Code for Facebook Track -->
<script type="text/javascript">
var fb_param = {};
fb_param.pixel_id = '6015211421988';
fb_param.value = '0.00';
fb_param.currency = 'USD';
(function(){
  var fpw = document.createElement('script');
  fpw.async = true;
  fpw.src = '//connect.facebook.net/en_US/fp.js';
  var ref = document.getElementsByTagName('script')[0];
  ref.parentNode.insertBefore(fpw, ref);
})();
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6015211421988&amp;value=0&amp;currency=USD" /></noscript>
</head>
<?php
//Edited 4-15-14 finished 11:40.  Added CSS in <head> and added buttons on ALL pages via CSS classname 1 & 2. Edited ISBN Table.  //Default text now displays and is removed via click.
/*
Template Name: SellYourBooks.debug
*/

/* 
Comment lines start with // for just one line or to comment out a bunch of lines, start a line with /* and all the lines after it will be comments until it reaches a line that has an * and a / together like the line which is about 10 lines below.  

below is from http://stackoverflow.com/questions/2810124/how-to-add-a-php-page-to-wordpress, the php start and end ? > had to be commented out below to keep this in here

You don't need to interact with the API, or use a plugin.

First, duplicate post.php or page.php in your theme folder (under /wp-content/themes/themename/).

Rename the new file as templatename.php (where templatename is what you want to call your new template!). Enter the following at the top of the new file:

this is where these files are for now 3/18/14
C:\Users\Scott\1ProgramsPHP\LoganSlone\Wordpress\wp-content\themes\xclusive\SellYourBooks.php
ftp://allegracorp.com/www/LoganSlone/WP/wp-content/themes/xclusive/SellYourBooks.php

< ?php
/*
Template Name: SellYourBooks.debug
*/
/*? >
You can modify this file (using php) to include other files or whatever you need.

Then create a new page in your wordpress blog, and in the page editing screen you'll see a 'Template' dropdown in the 'Attributes' widget to the right. Select your new template and publish the page.

Your new page will use the php code defined in templatename.php
*/

/**
 * XClusive - template for displaying all pages
 *
 * @file page.php
 * @package xclusive 
 * @author Hafid Trujillo
 * @copyright 2013 Xpark Media
 * @license license.txt 
 * @version release: 0.1.0
 * @filesource  wp-content/themes/xclusive/page.php
 * @since available since 0.1.0
 */
 

//*



/**************************************************************************
Comment lines start with // for just one line or to comment out a bunch of lines, start a line with /* and all the lines after it will be comments until it reaches a line that has an * and a / together like the line which is about 10 lines below.  

here is the database connection stuff that needs to be changed to fit your server.  Keep below in comments so you always have a template to work with.  Use the lines just below this commented section to customize for your server

if (!mysql_connect ('localhost', 'whetzels_usrLS', 'pasLogan')) {
	print "<P>ERROR CONNECTING TO DB" . mysql_error();
}

if (!mysql_select_db ("whetzels_LoganSlone")) {
	print "<P>ERROR SELECTING DB" . mysql_error();
}

//*/

if (!mysql_connect ('localhost', 'dmadmin', '6EKCDO4Qo7')) {
	print "<P>ERROR CONNECTING TO DB" . mysql_error();
}

if (!mysql_select_db ("sellyourbooks")) {
	print "<P>ERROR SELECTING DB" . mysql_error();
}


  get_header();


print '
	<section id="primary">

		<div id="content" role="main">
';


//*/




//below is true only in SellYourBooksDebug.php.  SellYourBooks.php is always the original.  Whenever you make changes to it, save a copy of it as SellYourBooksDebug.php with debug set to true like below and the lines above commented out.  Actually, I don't have debug version working yet and may never get one working, there is too much to change.  For now, the easiest thing is to keep one version and change just below.
$debug=true;


/* some isbns to try
9780805361179
9780321750815
9780321692177
9780805373738
9780321616401


these are better
9780132109178
9780321696724
9780321558237
9781118147290

with a dash which needs to be removed
978-0132109178
978-0321696724
978-0321558237
978-1118147290

get the isbn 10 version
9780132109178
9780321696724
9780321558237
9781118147290


these didn't work for Logan
9781454806240
9780314276872
9781599416410
9780735597891
9780735507470
9780314239440
9780735598225
but they did for me

ISBN			Cash Offer
9781454806240	Good Condition : $8.06
9780314276872	Good Condition : $91.74
9781599416410	Good Condition : $59.57
9780735597891	Good Condition : $19.00
9780735507470	Good Condition : $2.90
9780314239440	Good Condition : $4.42
9780735598225	Good Condition : $36.98

10 digit version of above
1454806249
0314276874
1599416417
0735597898
0735507473
0314239448
0735598223



//*/




if (isset($_POST['GetOffer'])) {
	//print "<br>isbns=" . $_POST['isbns'];
	$isbns=$_POST['isbns'];
	$verrormsg="";
	//if ($action=="Get Offer") {
	//if ($subaction=="Get Offer") {
	if ( ($isbns=="Enter ALL ISBNs  Example:  9783593812489 9784930948173   9784829577392   9783840948274") ) {
		$verrormsg="Please enter ISBN codes one per line in the box below.";
		?>


		<center>

		<form id="form1" name="form1" action="<?php echo get_permalink(); ?>" method="post">

		<TABLE>
		<TR>
			<TD colspan="2">
<P>Please enter the ISBN numbers of the books you wish to sell in the text box below.
			<P>Please enter them one per line and Press the Get Offer button to see our offer.
			<p align="center"><B><FONT face="Arial" COLOR="ff0000" size="2"><?php print $verrormsg;?></FONT></B></p>
			</TD>
		</TR>
		<TR>
			<TD>ISBNs:</TD>
			<TD><textarea cols="20" name="isbns" rows="12" wrap="virtual"><?php print $isbns;?></textarea>
			<P>&nbsp;
			<P><button name="GetOffer" class="classname" value="Get Prices">Get Prices</button>

			</TD>
		</TR>
		</TABLE>

		</form>

		</center>



	<?php
	}else{


	//if (($action=="Get Offer") && ($verrormsg=="")) {
	//if (($subaction=="Get Offer") && ($verrormsg=="")) {
	//if ($verrormsg=="") {

		//$msg="Hello,\n\nBelow is a message from the Contact Form (contact.php) on your site.\n\n" . "Name: $Name\n\nEmail: $FromEmail\n\nComments:\n$Comments";
		//I think this is Greg's copy of the email
		//mail($vemail, $Subject . " from Boat-World.com", $Comments . $XtraComments, "From: " . $FromEmail . "\nReply-To: " . $FromEmail, "-F$vfromemail");
		//mail($vemail, "Charity-Charities.org Contact Form Request From " . $FromEmail, $msg, "From: " . $FromEmail . "\nReply-To: " . $FromEmail);
		//above changed to below, now sent to sascha
		//mail($vemail, "Charity-Charities.org Contact Form Request From " . $FromEmail, $msg, "From: " . $FromEmail . "\nReply-To: " . $FromEmail);
		//mail("whetzels@verizon.net", "Charity-Charities.org Contact Form Request From " . $FromEmail, $msg, "From: " . $FromEmail . "\nReply-To: " . $FromEmail);


		//parse the isbns and put them in an array
		$isbnList=array();
		if ($debug) { print "<br>isbns=" . $isbns; }

		//parse text box, explode on \n or \r or \n\r, or use below?

		
		
		
		$isbns = str_replace("-", "", $isbns);
		$isbns = str_replace("\r\n", "\n", $isbns);
		$isbns = str_replace("\n\r", "\n", $isbns);		//i don't think this one is needed, but put it in anyway
		$isbns = str_replace("\r", "\n", $isbns);
		$isbnList = explode("\n", $isbns);
		
		//have to go backwards thru the list deleting the blank ones as you go
		for ($i=(sizeof($isbnList)-1); $i>=0; $i--) {
			//print "<br>isbn $i=" . $isbnList[$i];
			$isbnList[$i]=trim($isbnList[$i], " \t\0\x0B");
			if ($isbnList[$i]=="") {
				unset($isbnList[$i]);
			}
		}
		/*
		for ($i=0; $i<sizeof($isbnList); $i++) {
			print "<br>isbn $i=" . $isbnList[$i];
		}
		exit;
		//*/

		//get the amazon data for the isbns
		$CashOfferListGood=array();
		$CashOfferListAccept=array();
		$CashOfferListTxt=array();
		$LowesetUsed=array();
		$SalesRank=array();
		$Image=array();
		$Title=array();
		$Edition=array();
		$Author=array();
		$XmlOutput="";
		$ReturnedCount=0;
		//loop thru the list in batches of 10 until they are all processed
		for ($TenCount=0; $TenCount<(sizeof($isbnList)/10); $TenCount++) {
			if ($debug) { print "<br>TenCount=$TenCount."; }

			//get a batch of 10
			$isbn10=array();		//holds the current 10 isbns to be looked up
			$isbnstr="";	//holds the current 10 isbns in a string that will be added to the request url later
			//get the current 10 from isbnList
			for ($i=0; $i<10; $i++) {
				$index=( ($TenCount*10) + ($i) );
				if ($index < sizeof($isbnList)) {
					$isbn10[]=$isbnList[( ($TenCount*10) + ($i) )];
					$isbnstr .= $isbnList[( ($TenCount*10) + ($i) )] . " or ";
				}else{
					break;	// last batch of 10 which probably has less than 10
				}
			}
			$isbnstr = substr($isbnstr, 0, (strlen($isbnstr)-4) );		//remove last " or "
			$isbnstr =str_replace( ",", "%2C", $isbnstr);


			//prepare and execute the amazon api call
			$Locale="amazon.com";		

			/*$AWSAccessKeyId = "AKIAIPSYVM2FXZ3AHHTQ";
			$SecretAccessKey = "Xk1E/PZHwUf1giCcWT9sqgkAzagX4/zNG2XEhm0N";
			#above are your keys and below is your user login name
			$AssociateTag = "allegracorpco-20";/*/
			
			$AWSAccessKeyId = "AKIAIDFLEXUXQGAE6PRQ";
			$SecretAccessKey = "g99MawBR5NDOrgiN16G97nEaHEipkgxGLvGJMb+W";
			#above are your keys and below is your user login name
			$AssociateTag = "360buyback-20";

			//$ItemId = "0679722769"; // ASIN
			//$upc="012569768949";

			$Timestamp = gmdate("Y-m-d\TH:i:s\Z"); 
			//print $Timestamp; exit;
			$Timestamp = str_replace(":", "%3A", $Timestamp);
			//$ResponseGroup = "ItemAttributes,Offers,Images,Reviews";
			//$ResponseGroup = "SalesRank,ItemAttributes,OfferSummary";
			$ResponseGroup = "SalesRank,ItemAttributes,OfferSummary,Images";
			$ResponseGroup = str_replace(",", "%2C", $ResponseGroup);

			//notice above and below parms are in alphabetical order

			//$String="AWSAccessKeyId=" . $AWSAccessKeyId . "&AssociateTag=" . $AssociateTag . "&IdType=" . $IdType . "&ItemId=" . $IdTypeValue . "&Operation=ItemLookup&ResponseGroup=" . $ResponseGroup . "&SearchIndex=DVD&Service=AWSECommerceService&Timestamp=" . $Timestamp . "&Version=2011-08-01";
			//used all below 
			$String="AWSAccessKeyId=" . $AWSAccessKeyId . "&AssociateTag=" . $AssociateTag . "&IdType=" . $IdType . "&ItemId=" . $IdTypeValue . "&Operation=ItemLookup&ResponseGroup=" . $ResponseGroup . "&SearchIndex=All&Service=AWSECommerceService&Timestamp=" . $Timestamp . "&Version=2011-08-01";
			//above is what was in php program, below was in python program, since you are looking up 10, use below and convert to php
			//String="AWSAccessKeyId="+AWS_ACCESS_KEY_ID+"&AssociateTag=ASSOCIATE_TAG&Operation=ItemSearch&Power=ISBN:"+upcs+"&ResponseGroup="+ResponseGroup+"&SearchIndex=Books&Service=AWSECommerceService&Timestamp="+Timestamp+"&Version=2009-01-06"
			$String="AWSAccessKeyId=" . $AWSAccessKeyId . "&AssociateTag=" . $AssociateTag . "&Operation=ItemSearch&Power=ISBN:" . $isbnstr . "&ResponseGroup=" . $ResponseGroup . "&SearchIndex=Books&Service=AWSECommerceService&Timestamp=" . $Timestamp . "&Version=2011-08-01";

			$String = str_replace("\n", "", $String);
			//i added below from searchi when this code wasn't working, but I don't see any of the below chars in $String
			//but there must have been something because now this fucker works again, wtf!
			$String = str_replace("+", "%2B", $String);
			$String = str_replace(":", "%3A", $String);		//I put next 2 in for Power search
			$String = str_replace("*", "%2A", $String);
			$String = str_replace(" ", "%20", $String);

		
			$Prepend = "GET\nwebservices.". $Locale . "\n/onca/xml\n";
			$PrependString = $Prepend . $String;

			$Signature = base64_encode(hash_hmac("sha256", $PrependString, $SecretAccessKey, True));  
			$Signature = str_replace("+", "%2B", $Signature);
			$Signature = str_replace("=", "%3D", $Signature);
			//not sure if below (and above) should be all done before/after/both/split.  I tried all before and after and it failed.
			//todo, below are not in Python
			$Signature = str_replace(":", "%3A", $Signature);
			$Signature = str_replace("*", "%2A", $Signature);

			$BaseUrl = "http://webservices.". $Locale . "/onca/xml?";
			$SignedRequest = $BaseUrl . $String . "&Signature=" . $Signature;

			//$XML = simplexml_load_file($SignedRequest);

			//echo '<a href="'.$SignedRequest.'">XML</a><p>';
			$vurl = $SignedRequest;
			//print $vurl;
			//exit;
			//print_r ($XML);

			$curpage = @file_get_contents($vurl);
			$XmlOutput .= $curpage;


			/*for some reason above fails, but if I put vurl into browser, I get below?  Why? To see vurl, remove the @ above
			< ?xml version="1.0"? >
			<ItemLookupErrorResponse xmlns="http://ecs.amazonaws.com/doc/2011-08-01/"><Error><Code>AccountLimitExceeded</Code><Message>Account limit of 2000 requests per hour exceeded.</Message></Error><RequestID>093bc783-c9fd-4de0-ae71-84f20da5ab51</RequestID></ItemLookupErrorResponse>
			//*/

			
			if ($curpage==false) {
				//should never happen.  could keep going, but if this fails, amazon is down, your account is revoked, etc.  no sense in continuing.
				
        echo "Watch the page reload itself in 10 second!";
   
				exit;
			}

			

			//print "<br>Processing amazon query results ...";
			//process the amazon query results, parse the returned xml getting used price and rank for the 10 upcs request you just sent


			/*
			break up xml by each returned upc and search in that range for above

			could search backwards thru the results with $pos = stripos(strrev($haystack), strrev($needle)); but then lose correct position number?

			the returned xml has header stuff, and then the 10 blocks can be broken down into:

			<Item><ASIN>B001KKU9GE</ASIN><DetailPageURL>http://www.amazon.com/10-Dead-Men-Doug-Bradley/dp/B001KKU9GE%3FSubscriptionId%3DAKIAI3ISYLSZ6ENOPKKQ%26tag%3Dallegracorpco-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB001KKU9GE</DetailPageURL>

			<SalesRank>91716</SalesRank>

			<ItemAttributes><Actor>Doug Bradley</Actor><Actor>Pooja Shah</Actor><Actor>Brendan Carr</Actor><Actor>Terry Stone</Actor><Actor>Keith Eyles</Actor><AspectRatio>1.77:1</AspectRatio><AudienceRating>Unrated</AudienceRating><Binding>DVD</Binding><Brand>MTI PRODUCTIONS</Brand><Director>Ross Boyask</Director><EAN>0039414521207</EAN><Format>AC-3</Format><Format>Color</Format><Format>Dolby</Format><Format>DVD</Format><Format>Subtitled</Format><Format>Widescreen</Format><Format>NTSC</Format><Label>Mti Home Video</Label><Languages><Language><Name>English</Name><Type>Original Language</Type></Language><Language><Name>Spanish</Name><Type>Subtitled</Type></Language></Languages><ListPrice><Amount>2495</Amount><CurrencyCode>USD</CurrencyCode><FormattedPrice>$24.95</FormattedPrice></ListPrice><Manufacturer>Mti Home Video</Manufacturer><MPN>D2120D</MPN><NumberOfItems>1</NumberOfItems><PackageDimensions><Height Units="hundredths-inches">58</Height><Length Units="hundredths-inches">710</Length><Weight Units="hundredths-pounds">18</Weight><Width Units="hundredths-inches">542</Width></PackageDimensions><ProductGroup>DVD</ProductGroup><Publisher>Mti Home Video</Publisher><ReleaseDate>2009-01-27</ReleaseDate><RunningTime Units="minutes">90</RunningTime><Studio>Mti Home Video</Studio><TheatricalReleaseDate>2007</TheatricalReleaseDate><Title>10 Dead Men</Title>

			<UPC>039414521207</UPC>

			</ItemAttributes><OfferSummary><LowestNewPrice><Amount>1238</Amount><CurrencyCode>USD</CurrencyCode><FormattedPrice>$12.38</FormattedPrice></LowestNewPrice>

			<LowestUsedPrice><Amount>388</Amount>

			<CurrencyCode>USD</CurrencyCode>
			<FormattedPrice>$3.88</FormattedPrice>
			</LowestUsedPrice><TotalNew>38</TotalNew><TotalUsed>15</TotalUsed><TotalCollectible>0</TotalCollectible><TotalRefurbished>0</TotalRefurbished></OfferSummary></Item>

			//*/

		
			//loop thru the 10 isbns which were looked up on amazon and save the results to arrays
			for ($i=0; $i<sizeof($isbn10); $i++) {

				//the Amazon data is not always returned in the same order as the isbns that were submitted.  so lookup the isbns in the output in the same order they were entered so your looked up Amazon data is in the correct order

				if ($debug) { print "<P>searching for " . '&lt;EAN&gt;' . $isbn10[$i] . '&lt;/EAN&gt;'; }
				/*
				print "<br>searching for " . '<EAN>' . $isbn10[$i] . '</EAN>';
				$vsoibisbn = stripos($curpage, '<ISBN>' . $isbn10[$i] . '</ISBN>', $veoib);
				//it appears they strip off the 978 in the isbn tag, but the whole number is in the ean tag so just search for below instead*/
				//$vsoibisbn = stripos($curpage, '<EAN>' . $isbn10[$i] . '</EAN>', $veoib);
				$vsoibisbn = stripos($curpage, '<EAN>' . $isbn10[$i] . '</EAN>');
				if ($vsoibisbn === false) {
					$vsoibisbn = stripos($curpage, '<ISBN>' . $isbn10[$i] . '</ISBN>');
					if ($vsoibisbn === false) {
						//print '<P><B>====ERROR, ' . $isbn10[$i] . ' not found.</B>';
						$LowesetUsed[]="";
						$SalesRank[]="";
						$Image[]="";
						//$Title[]="ISBN " . $isbn10[$i] . " invalid/school specific version, please verify the ISBN entered is correct.";
						$Title[]="ISBN " . $isbn10[$i] . " invalid/school specific version, please correct and enter again.";
						$Edition[]="";
						$Author[]="";
						continue;
					}
				}

				//$vsoib = strrpos($curpage, '<Item>', $vsoibisbn);
				//strrpos is very confusing, offeset must be - to search backwards, but it is the number of chars from the END of the string
				$vsoib = strrpos($curpage, '<Item>', -(strlen($curpage) - $vsoibisbn) );
				if ($vsoib === false) {
					print '<P><B>====ERROR, &lt;Item&gt; not found.</B>';
					$LowesetUsed[]="";
					$SalesRank[]="";
					$Image[]="";
					continue;
				}

				#find end of this item block so you can verify you stay within it when searching
				$veoib = stripos($curpage, '</Item>', $vsoib);
				if ($veoib === false) {
					print '<P><B>====ERROR, &lt;/Item&gt; not found.</B>';
					$LowesetUsed[]="";
					$SalesRank[]="";
					$Image[]="";
					continue;
				}

				$ItemBlock=substr($curpage, $vsoib, ($veoib-$vsoib));
				//print "<br>vsoib=$vsoib, veoib=$veoib. vsoibisbn=$vsoibisbn"; // . substr($curpage, ($vsoib+11), ($vsoib - ($veoib+11) ) );

				//you may want to move ReturnedCount to after below fields are found, or just keep hear since the Item was found
				$ReturnedCount += 1;

				//get the sales rank
				$vstart = stripos($ItemBlock, '<SalesRank>');
				if ($vstart === false) {
					print '<P><B>====ERROR, &lt;SalesRank&gt; not found, skipping</B>';
					$SalesRank[]="";
				}else{
					$vend = stripos($ItemBlock, '</SalesRank>');
					if ($vstart === false) {
						print '<P><B>====ERROR, &lt;/SalesRank&gt; not found, skipping</B>';
						$SalesRank[]="";
					}else{
						$SalesRank[]=substr($ItemBlock, ($vstart+11), ($vend - ($vstart+11) ) );
					}
				}
				if ($debug) { print "<br>SalesRank=" . $SalesRank[$i] . "."; }

				
				//get the lowest used price
				$vstart = stripos($ItemBlock, '<LowestUsedPrice>');
				if ($vstart === false) {
					print '<P><B>====ERROR,<LowestUsedPrice&gt; not found, skipping</B>';
					$LowesetUsed[]="";
				}else{
					$vstart = stripos($ItemBlock, '<FormattedPrice>', $vstart);
					if ($vstart === false) {
						print '<P><B>====ERROR, &lt;FormattedPrice&gt; not found, skipping</B>';
						$LowesetUsed[]="";
					}else{
						$vend = stripos($ItemBlock, '</FormattedPrice>', $vstart);
						if ($vstart === false) {
							print '<P><B>====ERROR, &lt;/FormattedPrice&gt; not found, skipping</B>';
							$LowesetUsed[]="";
						}else{
							//added one to strlen below to skip the leading $ in formatted price
							$LowesetUsed[]=str_replace(',', '', substr($ItemBlock, ($vstart+17), ($vend - ($vstart+17) ) ) );

							/*python code for euros which i don't think we have to worry about euros
							#euros are 1.999,99 format so might as well fix this here
							if (curpage[(vend-3):(vend-2)]==","):
								vfieldsval[vfields[k]]=curpage[(vstart+17):(vend-3)].replace('.', '')+'.'+curpage[(vend-2):vend]
							else:
								vfieldsval[vfields[k]]=curpage[(vstart+17):vend].replace(',', '')
							//*/
						}
					}
				}
				if ($debug) { print "<br>UsedPrice=" . $LowesetUsed[$i] . "."; }


				//get the image
				$vstart = stripos($ItemBlock, '<MediumImage><URL>');
				if ($vstart === false) {
					print '<P><B>====ERROR, &lt;MediumImage&gt; not found, skipping</B>';
					$Image[]="";
				}else{
					$vend = stripos($ItemBlock, '</URL>', $vstart);
					if ($vstart === false) {
						print '<P><B>====ERROR, &lt;/URL&gt; not found, skipping</B>';
						$Image[]="";
					}else{
						$Image[]=substr($ItemBlock, ($vstart+18), ($vend - ($vstart+18) ) );
					}
				}
				if ($debug) { print "<br>Image=" . $Image[$i] . "."; }


				//get the Title
				$vstart = stripos($ItemBlock, '<Title>');
				if ($vstart === false) {
					print '<P><B>====ERROR, &lt;Title&gt; not found, skipping</B>';
					$Title[]="";
				}else{
					$vend = stripos($ItemBlock, '</Title>', $vstart);
					if ($vstart === false) {
						print '<P><B>====ERROR, &lt;/Title&gt; not found, skipping</B>';
						$Title[]="";
					}else{
						$Title[]=substr($ItemBlock, ($vstart+7), ($vend - ($vstart+7) ) );
					}
				}
				if ($debug) { print "<br>Title=" . $Title[$i] . "."; }


				//get the Edition
				$vstart = stripos($ItemBlock, '<Edition>');
				if ($vstart === false) {
					print '<P><B>====ERROR, &lt;Edition&gt; not found, skipping</B>';
					$Edition[]="";
				}else{
					$vend = stripos($ItemBlock, '</Edition>', $vstart);
					if ($vstart === false) {
						print '<P><B>====ERROR, &lt;/Edition&gt; not found, skipping</B>';
						$Edition[]="";
					}else{
						$Edition[]=substr($ItemBlock, ($vstart+9), ($vend - ($vstart+9) ) );
					}
				}
				if ($debug) { print "<br>Edition=" . $Edition[$i] . "."; }


				//get the Author
				$vstart = stripos($ItemBlock, '<Author>');
				if ($vstart === false) {
					print '<P><B>====ERROR, &lt;Author&gt; not found, skipping</B>';
					$Author[]="";
				}else{
					$vend = stripos($ItemBlock, '</Author>', $vstart);
					if ($vstart === false) {
						print '<P><B>====ERROR, &lt;/Author&gt; not found, skipping</B>';
						$Author[]="";
					}else{
						$Author[]=substr($ItemBlock, ($vstart+8), ($vend - ($vstart+8) ) );
					}
				}
				if ($debug) { print "<br>Author=" . $Author[$i] . "."; }

		
				/*keep these around in case you need them later
				//get the total used
				$vstart = stripos($curpage, '<TotalUsed>', $vsoib);
				if ($vstart === false) {
					print '<P><B>====ERROR, &lt;TotalUsed&gt; not found, skipping</B>';
				}else{
					$vend = stripos($curpage, '</TotalUsed>', $vstart);
					if ($vstart === false) {
						print '<P><B>====ERROR, &lt;/TotalUsed&gt; not found, skipping</B>';
					}else{
						$totalused=substr($curpage, ($vstart+11), ($vend - ($vstart+11) ) );
					}
				}
				//print "<P>rank=" . $rank . ".";

				//*/

			} //for ($i=0; $i<sizeof($isbn10); $i++) {	//loop thru the 10 isbns which were looked up on amazon and save the results to arrays
		} //for ($TenCount=0; $TenCount<(sizeof($isbnList)/10); $TenCount++) {	//loop thru the list in batches of 10 until they are all processed


		$OfferTotalGood=0;
		$OfferTotalAccept=0;
		//compute offer and display on the screen
		for ($i=0; $i<sizeof($isbnList); $i++) {

			if ($debug) { print "<P><B>For ISBN: " . $isbnList[$i] . "</B>"; }

			$CashOfferListTxt[$i]=$Title[$i];
			if ($Edition[$i] != "") {
				$CashOfferListTxt[$i] .= "\nEdition: " . $Edition[$i];
			}
			if ($Author[$i] != "") {
				$CashOfferListTxt[$i] .= "\n" . $Author[$i];
			}
		
			if ( ($SalesRank[$i] > 1000000) || ($SalesRank[$i] == "") ) {
				$CashOfferListGood[$i] = 0;
				$CashOfferListAccept[$i] = 0;
				continue;
			}



			//Column C, After Fees, in ssample sheet1 table1
			//=(D3*0.85)-5
			$CashOffer=0;
			if ($LowesetUsed[$i] > 6) {
				$CashOffer=($LowesetUsed[$i]*0.85)-5;
			}
			if ($debug) { print "<P>(LowesetUsed of " . $LowesetUsed[$i] . "*0.85)-5=" . $CashOffer . ", (0 if Lowest<6)."; }

			/*table lookup
			After Fee Price	-After Fee Price	-Amount subtracted	-Upper Margin %	-Lower Margin %
			-5	-0.01	0	0%	0%
			0	5.24	0	#DIV/0!	0%
			5.25	9.9900	3	133%	43%
			10	14.99	4	67%	36%
			15	19.99	5	50%	33%
			20	25.99	8	67%	44%
			26	35.99	10	63%	38%
			36	45.99	15	71%	48%
			46	55.99	20	77%	56%
			56	65.99	23	70%	54%
			70	79.99	25	56%	45%
			80	89.99	26	48%	41%
			90	99.99	30	50%	43%
			100	109.99	36	56%	49%
			110	119.99	40	57%	50%
			120	129.99	45	60%	53%
			130	139.99	50	63%	56%
			140	149.99	55	65%	58%
			150	159.99	60	67%	60%
			160	169.99	65	68%	62%
			170	179.99	70	70%	64%
			180	189.99	75	71%	65%
			190	199.99	80	73%	67%

			Column B, Cash Offer, in sample sheet1 table1
			=C2-VLOOKUP(C2,'Sheet 1 - Table 2'!$B$2:$E$12,4,TRUE)
			$CashOffer above - (lookup cash offer in the table to determine how much to subtract), that table is the if else block below
			//*/


			//After Fee table implemented below.  see comments a few lines down on how to change the table
			if ($CashOffer <= 0) {
				//do nothing, it is already 0 and won't be bought
			}elseif ($CashOffer < 5.24) {
				//do nothing, the offer isn't changed
			}elseif ($CashOffer < 9.99) {
				$CashOffer = ($CashOffer - 3);


			/*to change the table, add/delete/change the lines in groups of 2 like below
			the 14.99 is the upper range on each line in the table above.  the lower range is not need for each block because it was handled in the previous elseif
			the amount to subtract, 4, is listed on the second line
			that's all there is to it.  :)
			//*/
		/*The Following is the Original Table, commented out so that there would be record of what the original looked like.  4-18-14
			}elseif ($CashOffer < 14.99) {
				$CashOffer = ($CashOffer - 4);
			
			
			}elseif ($CashOffer < 19.99) {
				$CashOffer = ($CashOffer - 5);
			}elseif ($CashOffer < 25.99) {
				$CashOffer = ($CashOffer - 8);
			}elseif ($CashOffer < 35.99) {
				$CashOffer = ($CashOffer - 10);
			}elseif ($CashOffer < 45.99) {
				$CashOffer = ($CashOffer - 15);
			}elseif ($CashOffer < 55.99) {
				$CashOffer = ($CashOffer - 20);
			}elseif ($CashOffer < 65.99) {
				$CashOffer = ($CashOffer - 23);
			}elseif ($CashOffer < 79.99) {
				$CashOffer = ($CashOffer - 25);
			}elseif ($CashOffer < 89.99) {
				$CashOffer = ($CashOffer - 26);
			}elseif ($CashOffer < 99.99) {
				$CashOffer = ($CashOffer - 30);
			}elseif ($CashOffer < 109.99) {
				$CashOffer = ($CashOffer - 36);
			}elseif ($CashOffer < 119.99) {
				$CashOffer = ($CashOffer - 40);
			}elseif ($CashOffer < 129.99) {
				$CashOffer = ($CashOffer - 45);
			}elseif ($CashOffer < 139.99) {
				$CashOffer = ($CashOffer - 50);
			}elseif ($CashOffer < 149.99) {
				$CashOffer = ($CashOffer - 55);
			}elseif ($CashOffer < 159.99) {
				$CashOffer = ($CashOffer - 60);
			}elseif ($CashOffer < 169.99) {
				$CashOffer = ($CashOffer - 65);
			}elseif ($CashOffer < 179.99) {
				$CashOffer = ($CashOffer - 70);
			}elseif ($CashOffer < 189.99) {
				$CashOffer = ($CashOffer - 75);
			}elseif ($CashOffer < 199.99) {
				$CashOffer = ($CashOffer - 80);
			}
			$CashOfferAccept = ($CashOffer * .80);
			if ($debug) { print "<P>After Fee Table Good offer is now " . $CashOffer . "."; }
			if ($debug) { print "<P>After Fee Table Acceptable offer is now " . $CashOfferAccept . "."; }

//*/
}elseif ($CashOffer < 14.99) {
				$CashOffer = ($CashOffer - 4);
			
			
			}elseif ($CashOffer < 19.99) {
				$CashOffer = ($CashOffer - 5);
			}elseif ($CashOffer < 25.99) {
				$CashOffer = ($CashOffer - 7);
			}elseif ($CashOffer < 35.99) {
				$CashOffer = ($CashOffer - 8);
			}elseif ($CashOffer < 45.99) {
				$CashOffer = ($CashOffer - 10);
			}elseif ($CashOffer < 55.99) {
				$CashOffer = ($CashOffer - 14);
			}elseif ($CashOffer < 65.99) {
				$CashOffer = ($CashOffer - 15);
			}elseif ($CashOffer < 79.99) {
				$CashOffer = ($CashOffer - 20);
			}elseif ($CashOffer < 89.99) {
				$CashOffer = ($CashOffer - 25);
			}elseif ($CashOffer < 99.99) {
				$CashOffer = ($CashOffer - 28);
			}elseif ($CashOffer < 109.99) {
				$CashOffer = ($CashOffer - 32);
			}elseif ($CashOffer < 119.99) {
				$CashOffer = ($CashOffer - 35);
			}elseif ($CashOffer < 129.99) {
				$CashOffer = ($CashOffer - 40);
			}elseif ($CashOffer < 139.99) {
				$CashOffer = ($CashOffer - 50);
			}elseif ($CashOffer < 149.99) {
				$CashOffer = ($CashOffer - 55);
			}elseif ($CashOffer < 159.99) {
				$CashOffer = ($CashOffer - 60);
			}elseif ($CashOffer < 169.99) {
				$CashOffer = ($CashOffer - 65);
			}elseif ($CashOffer < 179.99) {
				$CashOffer = ($CashOffer - 70);
			}elseif ($CashOffer < 189.99) {
				$CashOffer = ($CashOffer - 75);
			}elseif ($CashOffer < 199.99) {
				$CashOffer = ($CashOffer - 80);
			}
			$CashOfferAccept = ($CashOffer * .80);
			if ($debug) { print "<P>After Fee Table Good offer is now " . $CashOffer . "."; }
			if ($debug) { print "<P>After Fee Table Acceptable offer is now " . $CashOfferAccept . "."; }



			/* Sales Rank percentage reduction and max offer limit table implemented below
			Sales Rank Lower	Sales Rank Upper	Percentage used to decrease offer		Maximum Offer Price
			2						74,999				1										120
			75,000					124,999				0.9										120
			125,000	174,999	0.88		120
			175,000	224,999	0.85		60
			225,000	274,999	0.8		55
			275,000	324,999	0.8		50
			325,000	399,999	0.75		45
			400,000	599,999	0.7		40
			600,000	749,000	0.65		35
			750,000	999,999	0.5		30
			1,000,000	999,999,999			0.25
			//*/

			//Sales Rank percentage reduction and max offer limit table implemented below.  see comments a few lines down on how to change the table
			if ($SalesRank[$i] < 74999) {
				//percentage is 100% so no change, just check max offer
				$MaxOffer = 120;
				$SalesRankPer = (1.0);
			}elseif ($SalesRank[$i] < 124999) {
				$MaxOffer = 120;
				$SalesRankPer = (0.9);


			/*to change the table, add/delete/change the lines in groups of 5 like below
			the 174999 is the upper range on each line in the table above.  the lower range is not need for each block because it was handled in the previous elseif
			the percentage amount to multiply by, 0.88, is listed on the second line
			the maximum offer price is listed on the third and fourth lines and must be changed in both places
			that's all there is to it.  :)
			//*/
			}elseif ($SalesRank[$i] < 174999) {
				$MaxOffer = 120;
				$SalesRankPer = (0.88);


			}elseif ($SalesRank[$i] < 224999) {
				$MaxOffer = 65;
				$SalesRankPer = (0.85);
			}elseif ($SalesRank[$i] < 274999) {
				$MaxOffer = 60;
				$SalesRankPer = (0.84);
			}elseif ($SalesRank[$i] < 324999) {
				$MaxOffer = 55;
				$SalesRankPer = (0.84);
			}elseif ($SalesRank[$i] < 399999) {
				$MaxOffer = 50;
				$SalesRankPer = (0.8);
			}elseif ($SalesRank[$i] < 599999) {
				$MaxOffer = 45;
				$SalesRankPer = (0.7);
			}elseif ($SalesRank[$i] < 749000) {
				$MaxOffer = 44;
				$SalesRankPer = (0.65);
			}elseif ($SalesRank[$i] < 999999) {
				$MaxOffer = 30;
				$SalesRankPer = (0.5);
			}
			$CashOffer = ($CashOffer * $SalesRankPer);
			$CashOfferAccept = ($CashOfferAccept * $SalesRankPer);
			if ($debug) { print "<P>Sales Rank % reduction, Good offer is now " . $CashOffer . "."; }
			if ($debug) { print "<P>Sales Rank % reduction, Accept offer is now " . $CashOfferAccept . "."; }

			
			//round to nearest 25 cents, In excel, =ROUND(A1/.25,0)*.25,  The formula divides the original value by .25 and then multiplies the result by .25. You can, of course, use a similar formula to round values to other fractions. For example, to round a dollar amount to the nearest nickel, simply substitute .05 for each of the two occurrences of ".25" in the preceding formula.
			$CashOffer = round($CashOffer, 0 , PHP_ROUND_HALF_UP);
			$CashOfferAccept = round($CashOfferAccept, 0 , PHP_ROUND_HALF_UP);
			if ($debug) { print "<P>Round to nearest .25 cents, Good offer is now " . $CashOffer . "."; }
			if ($debug) { print "<P>Round to nearest .25 cents, Accept offer is now " . $CashOfferAccept . "."; }


			//Take an additional %15 off the Good condition price and choose the lesser of that or the $MaxOffer price in the Sales Rank table above
			//$CashOffer85 = $CashOffer * 0.85;
			//if ($debug) { print "<P>Good condition - 15% offer is now " . $CashOffer85 . "."; }
			//if ($CashOffer85 > $MaxOffer) {
			if ($debug) { print "<P>The lesser of Good $CashOffer and $MaxOffer is "; }
			if ($CashOffer > $MaxOffer) {
				$CashOffer = $MaxOffer;
			/*
			}else{
				$CashOffer = $CashOffer85;
			//*/
			}
			if ($debug) { print " $CashOffer."; }


			if ($debug) { print "<P>The lesser of Accept $CashOfferAccept and $MaxOffer is "; }
			if ($CashOfferAccept > $MaxOffer) {
				$CashOfferAccept = $MaxOffer;
			}
			if ($debug) { print " $CashOfferAccept."; }


			if ($CashOffer <= 0) {
				$CashOfferListGood[$i] = 0;
				$CashOfferListAccept[$i] = 0;
			}elseif ($CashOffer > 199.99) {  //this should never execute because of sales rank table above, but left it in anyway
				$CashOfferListGood[$i] = 199.99;
				$CashOfferListAccept[$i] = 199.99;
			}else{
				$CashOfferListGood[$i] = $CashOffer;
				$CashOfferListAccept[$i] = $CashOfferAccept;
				$OfferTotalGood += $CashOffer;
				$OfferTotalAccept += $CashOfferAccept;
			}
		} //for ($i=0; $i<sizeof($isbnList); $i++) {	//compute offer and display on the screen


		//after all the books have been processed, if total offer > 10, go back through all the offers and change the 0s to .25 and add that back in total.
		if ($OfferTotalGood > 10) {
			for ($i=0; $i<sizeof($CashOfferListGood); $i++) {
				if (($CashOfferListGood[$i] == 0) && (stripos($Title[$i], "invalid/school") === FALSE) ) {
					$CashOfferListGood[$i]=.25;
					$OfferTotalGood += .25;
				}
			}
		}

		if ($OfferTotalAccept > 10) {
			for ($i=0; $i<sizeof($CashOfferListAccept); $i++) {
				if (($CashOfferListAccept[$i] == 0) && (stripos($Title[$i], "invalid/school") === FALSE) ) {
					$CashOfferListAccept[$i]=.25;
					$OfferTotalAccept += .25;
				}
			}
		}


		$OfferText="";
		for ($i=0; $i<sizeof($isbnList); $i++) {
			$OfferText .=  $isbnList[$i] . ' - ' . $CashOfferListTxt[$i] . '\nGood: $' . number_format($CashOfferListGood[$i], 2) . ', Accept: $' . number_format($CashOfferListAccept[$i], 2) . "\n\n";
		}
		$OfferText .=  '--- Total Good: $' . number_format($OfferTotalGood, 2) . ',   Accept: $' . number_format($OfferTotalAccept, 2) . "\n";


		//save data to news db.
		$sqls="INSERT INTO OffersLog (TimeStamp, OfferText, OfferTotalGood, OfferTotalAccept, LookupCount, ReturnedCount, XmlOutput) values (\"" . date("Y-m-d H:i:s") . "\", \"" . str_replace('"', '\"', $OfferText) . "\", " . $OfferTotalGood . ", " . $OfferTotalAccept . ", " . sizeof($isbnList) . ", " . $ReturnedCount . ", \"" . str_replace('"', '\"', $XmlOutput) . "\" )";
		$resins = mysql_query ($sqls); 
		if ($debug) { print "<p>resins=" . $resins . " sqls=" . $sqls; }
		if (mysql_affected_rows()==1) {
			$recordid=mysql_insert_id ();
			if ($debug) { print "<BR>Added successfully as record ID: $recordid."; }
		}else{
			echo "<p><b>ERROR adding.</b> ", mysql_error();
		}


		//display offers
		print '
			<center>
			<TABLE cellspacing="5" cellpadding="5">
			<TR>
				<TD align="center"><B>ISBN Offer</B></TD>
				<TD><B>Book Photo</B></TD>
			</TR>
		';
		
		for ($i=0; $i<sizeof($isbnList); $i++) {
			print '
				<TR>
					<TD valign="top">' . $isbnList[$i] . '<br>' . nl2br($CashOfferListTxt[$i]) . '<br>Good: $' . number_format($CashOfferListGood[$i], 2) . ', Acceptable: $' . number_format($CashOfferListAccept[$i], 2) . '
					</TD>
					<TD valign="top"><IMG SRC="' . $Image[$i] . '" BORDER=0 ALT=""></TD>
				</TR>
			';
		}

		print '
			<tr class="top">
				<TD>Total Good: $' . number_format($OfferTotalGood, 2) . ', Total Acceptable: $' . number_format($OfferTotalAccept, 2) . '</TD>
				<TD>&nbsp;</TD>
			</TR>
		';

		print '
			
			
			</TABLE>

			<br>&nbsp;




			<form id="form1" name="form1" action="' . get_permalink() . '" method="post">
			<TABLE>
			<TR>
				<TD>
				<input type="hidden" name="recordid" value="' . $recordid . '">
				<button name="SellBooks" class="classname" value="Sell">Sell Books</button>
				</TD>
			</TR>
			</TABLE>
			</form>

			</center>

			</BODY>
			</HTML>
		
		';

	}








//user enter isbns, likes the prices and wants to sell their books, so display form and get their contact info
}elseif (isset($_POST['SellBooks'])) {
	$recordid=$_POST['recordid'];
?>

	<center>

	<P>Please enter your contact info and Press the Contact Me button.
	<BR>Fields with <B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B> are required.

	<p align="center"><B><FONT face="Arial" COLOR="ff0000" size="2"><?php print $verrormsg;?></FONT></B></p>

	<form id="form1" name="form1" action="<?php echo get_permalink(); ?>" method="post">

	<TABLE>
	<TR>
		<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>First and Last Name:</TD>
		<TD><input maxLength="50" name="Name" size="50" value="<?php print $Name;?>"></TD>
	</TR>

	<TR>
		<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>Email:</TD>
		<TD><input maxLength="50" name="Email" size="50" value="<?php print $Email;?>"></TD>
	</TR>

	<TR>
		<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>Cell phone number<BR>(so we can arrange meet up):</TD>
		<TD><input maxLength="50" name="Cell" size="50" value="<?php print $Cell;?>"></TD>
	</TR>

	<TR>
		<TD align="right">How did you hear about us?:</TD>
		<TD>
		<select name="ReferredBy">
		<option value="" <?php if ($ReferredBy=="") { print " selected "; } ?> >Select</option>
		<option value="Facebook" <?php if ($ReferredBy=="Facebook") { print " selected "; } ?> >Facebook</option>
		<option value="Friend" <?php if ($ReferredBy=="Friend") { print " selected "; } ?> >From a friend</option>
		<option value="Flyer" <?php if ($ReferredBy=="Flyer") { print " selected "; } ?> >Flyer/Poster</option>
		<option value="GreekLife" <?php if ($ReferredBy=="GreekLife") { print " selected "; } ?> >Greek Life Fraternity/Sorority</option>
		<option value="Email" <?php if ($ReferredBy=="Email") { print " selected "; } ?> >Received email</option>
		<option value="Other" <?php if ($ReferredBy=="Other") { print " selected "; } ?> >Other</option>
		</select>	
		</TD>
	</TR>

	<TR>
		<TD align="right">Prefer to meet:</TD>
		<TD>
		<input type="radio" name="Meet" value="OnCampus" <?php if ($Meet=="OnCampus") { print " checked "; } ?> >On-Campus<br>
		<input type="radio" name="Meet" value="OffCampus" <?php if ($Meet=="OffCampus") { print " checked "; } ?> >Off-Campus
		</TD>
	</TR>

	<TR>
		<TD>&nbsp;</TD>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
		<TD>
		<input type="hidden" name="recordid" value="<?php print $recordid;?>">
		<button name="ContactMe" class="classname1" value="Sell">Submit Books</button>
		</TD>
	</TR>
	</TABLE>

	</form>

	</center>











<?php
}elseif (isset($_POST['ContactMe'])) {
	$verrormsg="";
	$Name=$_POST['Name'];
	$Email=$_POST['Email'];
	$Cell=$_POST['Cell'];
	$ReferredBy=$_POST['ReferredBy'];
	$Meet=$_POST['Meet'];
	$recordid=$_POST['recordid'];
	if ( ($Name=="") || ($Email=="") || ($Cell=="") ) {
		$verrormsg="Missing: ";

		if ($Name=="") {
			$verrormsg .= "Name, ";
		}

		if ($Email=="") {
			$verrormsg .= "Email, ";
		}

		if ($Cell=="") {
			$verrormsg .= "Cell, ";
		}

		//remove last ", "
		$verrormsg=substr($verrormsg, 0, strlen($verrormsg)-2) . ".";
		?>


		<center>

		<P>Please enter your contact info and Press the Contact Me button.
		<BR>Fields with <B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B> are required.

		<p align="center"><B><FONT face="Arial" COLOR="ff0000" size="2"><?php print $verrormsg;?></FONT></B></p>

		<form id="form1" name="form1" action="<?php echo get_permalink(); ?>" method="post">

		<TABLE>
		<TR>
			<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>First and Last Name:</TD>
			<TD><input maxLength="50" name="Name" size="50" value="<?php print $Name;?>"></TD>
		</TR>

		<TR>
			<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>Email:</TD>
			<TD><input maxLength="50" name="Email" size="50" value="<?php print $Email;?>"></TD>
		</TR>

		<TR>
			<TD align="right"><B><FONT face="Arial" COLOR="ff0000" size="2">*</FONT></B>Cell phone number<BR>(so we can arrange meet up):</TD>
			<TD><input maxLength="50" name="Cell" size="50" value="<?php print $Cell;?>"></TD>
		</TR>

		<TR>
			<TD align="right">How did you hear about us?:</TD>
			<TD>
			<select name="ReferredBy">
			<option value="" <?php if ($ReferredBy=="") { print " selected "; } ?> >Select</option>
			<option value="Facebook" <?php if ($ReferredBy=="Facebook") { print " selected "; } ?> >Facebook</option>
			<option value="Friend" <?php if ($ReferredBy=="Friend") { print " selected "; } ?> >From a friend</option>
			<option value="Flyer" <?php if ($ReferredBy=="Flyer") { print " selected "; } ?> >Flyer/Poster</option>
			<option value="FratSor" <?php if ($ReferredBy=="FratSor") { print " selected "; } ?> >Greek Life Fraternity/Sorority</option>
			<option value="Email" <?php if ($ReferredBy=="Email") { print " selected "; } ?> >Received email</option>
			<option value="Other" <?php if ($ReferredBy=="Other") { print " selected "; } ?> >Other</option>
			</select>	
			</TD>
		</TR>

		<TR>
			<TD align="right">Prefer to meet:</TD>
			<TD>
			<input type="radio" name="Meet" value="OnCampus" <?php if ($Meet=="OnCampus") { print " checked "; } ?> >On-Campus<br>
			<input type="radio" name="Meet" value="OffCampus" <?php if ($Meet=="OffCampus") { print " checked "; } ?> >Off-Campus
			</TD>
		</TR>

		<TR>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
		</TR>
		<TR>
			<TD>&nbsp;</TD>
			<TD>
			<input type="hidden" name="recordid" value="<?php print $recordid;?>">
			<button name="ContactMe" class="classname1" value="Submit Books">Submit Books</button>
			</TD>
		</TR>
		</TABLE>

		</form>

		</center>


	<?php
	}else{
		$sqls="UPDATE OffersLog set Name=\"". $Name . "\", Email=\"". $Email . "\", Cell=\"". $Cell . "\", ReferredBy=\"". $ReferredBy . "\", Meet=\"". $Meet . "\" where ID=". $recordid;	
		$result = mysql_query ($sqls); 
		//print ("res=" . $result . " sqls=" . $sqls);
		if (mysql_affected_rows()>=0) {
			//print ("<BR><P>" . mysql_affected_rows() . " record(s) updated for " . $table2edit . " number " . $recordid . ".");
		}else{
		   echo "<P>Error, UPDATE failed: ", mysql_error();
		   // exit; to where? todo
		}


		$sqls="select OfferText from OffersLog where ID=$recordid";
		$result = mysql_query($sqls);
		//print ("res=" . $result . " sqls=" . $sqls);
		$row = mysql_fetch_array($result);

		/*
		5) This form should then be sent with all the information that they filled out, as well as a list of the ISBN, Title, and Price for each book, the totals, and breakdown of the multi book bonus and total for that as well.
		//*/

		$msg="Name: $Name\nEmail: $Email\nCell:$Cell\nReferredBy:$ReferredBy\nMeet:$Meet\nOffer Details:\n" . $row["OfferText"] . "Record ID:$recordid\n" . "\n\nEnd of email";
		//mail("whetzels1@gmail.com", "Cash For Books Request from 365buyback .com", $msg, "From: " . $Email . "\nReply-To: " . $Email, "-F$vEmail");
		
		$To="sales@360buyback.com";
	
		mail($To, "Customer Submission", $msg, "From: " . $Email . "\nReply-To: " . $Email);




$msg="Hello $Name,\n\nThank you for submitting your books at 360BuyBack.com and supporting a 100% student owned and run business.\n\nPlease verify that $Cell is your correct number, you should be recieving a text from us shortly to arrange a convenient time to meet up for your books. \n\n" . "Here is the information we recieved from you:\nName: $Name\nEmail: $Email\nCell:$Cell\nReferredBy:$ReferredBy\nMeet:$Meet\nOffer Details:\n" . $row["OfferText"] . "\n\nSincerely,\n 360 Buyback";
		//mail("whetzels1@gmail.com", "Cash For Books Request from 365buyback .com", $msg, "From: " . $Email . "\nReply-To: " . $Email, "-F$vEmail");
		
		
		$To="$Email";
		
		mail($To, "Books Submitted - 360BuyBack!", $msg, "From: " . "books@360buyback.com" . "\nReply-To: " . $Email);

		//display offers
		print '
		<BODY>

		<center>

		<P>Thank you for giving us the opportunity to purchase your books. 
		<p> We are a local company 100% owned and run by college students.  We will contact you within 24 hours or less.
		If you do not recieve an email from us immediately there may have been a problem because of 1 or 2 reasons...
		<p>1) The email address you provided was invalid 
		<p>2) Something went wrong with our program (beta version)
		<p>Please shoot us an email <a href="mailto:service@360buyback.com?subject=Possible Error">service@360BuyBack.com</a> and we will personally run your books through the system manually.
		
		<p>Thanks again for using 360BuyBack.com  Be sure to tell your friends about us!
		<p><a href="https://www.facebook.com/365buyback">Like our FB Page!</a>for an extra 5% on your order. 

		</center>

		</BODY>
		</HTML>
		';


	}








}else{
?>

	<center>

	<form id="form1" name="form1" action="<?php echo get_permalink(); ?>" method="post">

	<TABLE>
	<TR>
		<TD colspan="2">

<html>
<body>
<p><font size="5"> Price Your Books</font> </p>
<p> 1) Enter all ISBN numbers (all at once) and click "Get Prices" </p>
<p> 2) Review offer page and click "Sell Books" </p>
<p> 3) Meet up and get cash!!</p>
<img src="http://www.kindercare.com/images/ReadShareGive_isbn_location.png
" alt="W3Schools.com" width="254" height="120">
</p>


		<p align="center"><B><FONT face="Arial" COLOR="ff0000" size="2"><?php print $verrormsg;?></FONT></B></p>
		</TD>
</TR>
	<TR>
		
		<TD><textarea cols="20" name="isbns" rows="8" wrap="virtual" onclick="document.getElementById('area').innerHTML='';" id="area"><?php print $isbns;?>Enter ALL ISBNs  Example:  9783593812489 9784930948173   9784829577392   9783840948274</textarea>
		<P>&nbsp;
		<P><button name="GetOffer" class="classname" value="Get Prices">Get Prices</button>
		</TD>
	</TR>
	</TABLE>

	</form>

	</center>


<?php
}
?>


<?php
/*below is from the original page.php, but modified so I could easiy comment out.  decided to remove the while and the comments part to make the page look better.
			while ( have_posts() ) : the_post();
				get_template_part( 'content', 'page');
                comments_template( '', true );
			endwhile;
//*/
?>

		</div><!--#content-->
	</section><!--#primary-->



  <?php get_footer(); ?>  
</body>
</html>