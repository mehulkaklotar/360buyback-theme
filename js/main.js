/**
 * Created by spock on 5/1/16.
 */

jQuery(document).ready(function($){

    $(document).on('click','.book-delete',function(e){
        e.preventDefault();
        _tr = $(this).parent().parent();
        isbn = _tr.data('isbn');
        good = _tr.data('good');
        accept = _tr.data('accept');

        _good = $('#book-offer-good');
        _good.text(_good.text()-good);

        _accept = $('#offer-accept-book');
        _accept.text(_accept.text()-accept);

        _isbn = $('#book-isbns');
        if (_isbn.val()){
            data = _isbn.val().split(',');
            index =data.indexOf(String(isbn));
            if (index > -1) {
                data.splice(index, 1);
            }
            _isbn.val(data.join());
            $('#book-isbns-add-more').val(_isbn.val());
        }
        _tr.remove();
    });

    var $form = $("#price-books-form");
    $("#add-more-books").colorbox({inline:true, href:$form,width:"50%"});

    /*$(document).on('click','#add-more-books',function(e){
        e.preventDefault();
        /!*$('#price-books-form').show();*!/
    });*/
});