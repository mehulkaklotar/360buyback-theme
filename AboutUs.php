<html>
<body>
<?php
/*
Template Name: AboutUs
*/
/**
 * XClusive - template for displaying all pages
 *
 * @file page.php
 * @package xclusive 
 * @author Hafid Trujillo
 * @copyright 2013 Xpark Media
 * @license license.txt 
 * @version release: 0.1.0
 * @filesource  wp-content/themes/xclusive/page.php
 * @since available since 0.1.0
 */
 
  get_header(); ?>
 	<h1><font size="6.7"<Strong>About Us</Strong></font></h1> 
<hr></hr>
<br>
<br>
	<h4><font size="5"><strong>Our Story</strong></font></h4>
		<p>
			<strong><font size="4" color="blue">360BuyBack</strong></font>		 was founded in 2011 by a UNF student who 			realized that students lacked a good way to sell back their textbooks. At the end of each semester he noticed that students went through the same problems when trying to sell back books:
<br>
<br>
		<font size="3"> <Strong>Book Store</a></strong></font>
			<li>Bookstore wasn't buying back their books</li>
			<li>Long lines</li>
			<li>Few books eligible for buyback</li>
			<li>Low prices</li>
			<li>Impersonal service</li>

<br>
<br>
			<font size="3"><strong>Online Buybacks</strong></font>
				<li>Had to ship books in</li>
				<li>Takes weeks to months to receive the money</li>
				<li>Company may mail book back for not being "Good" condition</li>
<br>
<br>

<h5><font size="5"><strong>Our Solution</font></strong></h5>
	<br>
		
		Provide students with a website where they can get our instant buyback price for their book.  They fill out a simple for to arrange a meetup for the books.  One of our sales reps gets in contact with the student to buy his/her books and give them CASH on the spot.  No waiting in lines, no shipping books and waiting weeks for the money.
		
	<br<>
	We base our prices of the going market value of the specific book.  

</ul>
<br>
<br>
It is stressful enough having to deal with finals and preparing to go home! We set out to create the most convenient textbook market by hiring student buyers who could travel to customers throughout the day and pay cash for their books. Our customers loved it!  They could request a book pickup as they prepared to leave school, relieving the hassle of one more chore.
Since then, we have proven that a business built by motivated students can make an impact.
We have paid University of North Florida students thousands of dollars for their used books.</p>

<br /><br /><br />

			<div id="content" role="main">
			
				
               
			
			
			
		</div><!--#content-->
	</section><!--#primary-->

 </body>
 </html>
